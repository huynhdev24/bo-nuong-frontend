import { LOAD_SUCCESS } from '../constants';

export const cartReducer = (state, action) => {
  const {
    type,
    payload: { cart },
  } = action;

  switch (type) {
    case LOAD_SUCCESS:
      return {
        ...state,
        cartLoading: false,
        cart,
      };

    default:
      return state;
  }
};
