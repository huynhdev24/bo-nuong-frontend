import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  getLatestProducts,
  getProducts,
  getDiscountProducts,
} from '../redux/actions/products';
import { getCategories } from '../redux/actions/categories';
import { categories$, products$ } from '../redux/selectors';
import LatestProducts from '../components/products/LatestProducts';
import ListCategories from '../components/products/ListCategories';
import { useContext } from 'react';
import { CartContext } from '../contexts/CartContext';
import { toast$ } from '../redux/selectors';
import Tooltip from '../components/layouts/Tooltip';
import Header from '../components/products/Header';
import Discount from '../components/products/Discount';
import ListProducts from '../components/products/ListProducts';
import PriceRangeSlider from '../components/products/PriceRangeSlider';

const Products = () => {
  const dispatch = useDispatch();
  const categories = useSelector(categories$);
  const products = useSelector(products$);
  const { addCart } = useContext(CartContext);
  const toast = useSelector(toast$);
  const [minValue, setMinValue] = useState(0);
  const [maxValue, setMaxValue] = useState(2500000);

  useEffect(() => {
    if (categories.data.length === 0) {
      dispatch(getCategories.getCategoriesRequest());
    }
    if (products.latestProducts.length === 0) {
      dispatch(getLatestProducts.getLatestProductsRequest());
    }
    if (products.discountProducts.length === 0) {
      dispatch(getDiscountProducts.getDiscountProductsRequest());
    }
    if (products.data.length === 0) {
      dispatch(getProducts.getProductsRequest());
    }
  }, [
    categories.data.length,
    dispatch,
    products.data.length,
    products.discountProducts.length,
    products.latestProducts.length,
  ]);

  const onAddCart = (product) => {
    addCart(product);
  };

  return (
    <>
      <Tooltip toast={toast} />
      {/* <!-- Breadcrumb Section Begin --> */}
      <Header />
      {/* <!-- Breadcrumb Section End -->

    <!-- Product Section Begin --> */}
      <section className="product spad">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-5">
              <div className="sidebar">
                <LatestProducts products={products} />
                <PriceRangeSlider
                  minValue={minValue}
                  setMinValue={setMinValue}
                  maxValue={maxValue}
                  setMaxValue={setMaxValue}
                />
                <ListCategories categories={categories} />
              </div>
            </div>
            <div className="col-lg-9 col-md-7">
              <Discount products={products} onAddCart={onAddCart} />
              <ListProducts
                products={products}
                onAddCart={onAddCart}
                minValue={minValue}
                maxValue={maxValue}
              />
            </div>
          </div>
        </div>
      </section>
      {/* <!-- Product Section End --> */}
    </>
  );
};

export default Products;
