import EditIcon from '@mui/icons-material/Edit';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { CircularProgress } from '@mui/material';
import { GridActionsCellItem } from '@mui/x-data-grid';
import { useCallback, useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import AddModal from '../../components/orders/AddModal';
import DataTable from '../../components/layouts/DataTable';
import { AuthContext } from '../../contexts/AuthContext';
import { setCurrentId, showModal } from '../../redux/actions';
import { getOrders } from '../../redux/actions/orders';
import { orders$, toast$ } from '../../redux/selectors';
import moment from 'moment';
import 'moment/locale/vi';
import DetailModal from '../../components/orders/DetailModal';
moment.locale('vi');

const AdminOrders = () => {
  const dispatch = useDispatch();
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const toast = useSelector(toast$);
  const orders = useSelector(orders$);
  const [open, setOpen] = useState(false);
  const [selectedId, setSelectedId] = useState('');
  const {
    authState: { user },
  } = useContext(AuthContext);

  useEffect(() => {
    if (orders.data.length === 0) {
      dispatch(getOrders.getOrdersRequest());
    }
  }, [dispatch, orders.data.length]);

  const setShowModal = useCallback(() => {
    dispatch(showModal());
  }, [dispatch]);

  const handleChangeRowsPerPage = (newPageSize) => {
    setRowsPerPage(newPageSize);
  };

  if (user?.role !== 'ADMIN') {
    return <Redirect to="/404" />;
  }
  if (orders.loading) {
    return (
      <div
        style={{
          left: '50%',
          top: '50%',
          transform: 'translateY(-50%)',
          position: 'absolute',
        }}
      >
        <CircularProgress />
      </div>
    );
  }

  const handleEditClick = (id) => (event) => {
    event.stopPropagation();
    dispatch(setCurrentId(id));
  };

  const handleClose = () => {
    setSelectedId('');
    setOpen(false);
  };

  const handleSeeDetailClick = (id) => (event) => {
    event.stopPropagation();
    setSelectedId(id);
    setOpen(true);
  };

  const columns = [
    { field: 'id', headerName: 'Mã đơn', minWidth: 120, flex: 1 },
    {
      field: 'isDelivery',
      headerName: 'Trạng thái giao hàng',
      width: 80,
      flex: 1,
      valueGetter: (param) => {
        return param.value ? 'Đã giao' : 'Chưa giao';
      },
    },
    {
      field: 'isPayment',
      headerName: 'Trạng thái thanh toán',
      width: 80,
      flex: 1,
      valueGetter: (param) => {
        return param.value ? 'Đã thanh toán' : 'Chưa thanh toán';
      },
    },
    {
      field: 'totalPrice',
      headerName: 'Tổng tiền',
      width: 100,
      flex: 1,
      valueGetter: (param) => {
        return `${String(param.value).replace(/(.)(?=(\d{3})+$)/g, '$1,')} ₫`;
      },
    },
    {
      field: 'user',
      headerName: 'Người đặt',
      width: 150,
      flex: 3,
      valueGetter: (param) => {
        const user = param.value;
        return `${user.fullName} - ${user.phone} - ${user.address}`;
      },
    },
    {
      field: 'createdAt',
      headerName: 'Ngày tạo',
      type: 'date',
      width: 60,
      flex: 1,
      valueGetter: (param) => {
        return `${moment(param.value).format('l')}`;
      },
    },
    {
      field: 'actions',
      type: 'actions',
      headerName: 'Thao tác',
      width: 60,
      flex: 1,
      cellClassName: 'actions',
      getActions: ({ id }) => [
        <GridActionsCellItem
          icon={<VisibilityIcon color="info" />}
          label="Watch"
          className="textPrimary"
          onClick={handleSeeDetailClick(id)}
          color="inherit"
        />,
        <GridActionsCellItem
          icon={<EditIcon color="warning" />}
          label="Edit"
          className="textPrimary"
          onClick={handleEditClick(id)}
          color="inherit"
        />,
      ],
    },
  ];

  return (
    <>
      <DetailModal open={open} id={selectedId} handleClose={handleClose} />
      <DataTable
        component={AddModal}
        toast={toast}
        data={orders.data}
        columns={columns}
        rowsPerPage={rowsPerPage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        setShowModal={setShowModal}
      />
    </>
  );
};

export default AdminOrders;
