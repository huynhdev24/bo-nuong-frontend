//import DeleteForeverRoundedIcon from '@mui/icons-material/DeleteForeverRounded';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import EditIcon from '@mui/icons-material/Edit';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import { CircularProgress } from '@mui/material';
import { GridActionsCellItem } from '@mui/x-data-grid';
import { useCallback, useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import AddModal from '../../components/customers/AddModal';
import DataTable from '../../components/layouts/DataTable';
//import DeleteButton from '../../components/layouts/DeleteButton';
import { AuthContext } from '../../contexts/AuthContext';
import { setCurrentId, showModal } from '../../redux/actions';
import { getCustomers } from '../../redux/actions/customers';
import { customers$, toast$ } from '../../redux/selectors';

const AdminCustomers = () => {
  const dispatch = useDispatch();
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const toast = useSelector(toast$);
  const customers = useSelector(customers$);
  // const [open, setOpen] = useState(false);
  // const [selectedId, setSelectedId] = useState('');
  const {
    authState: { user },
  } = useContext(AuthContext);

  useEffect(() => {
    if (customers.data.length === 0)
      dispatch(getCustomers.getCustomersRequest('user'));
  }, [customers.data.length, dispatch]);

  const setShowModal = useCallback(() => {
    dispatch(showModal());
  }, [dispatch]);

  const handleChangeRowsPerPage = (newPageSize) => {
    setRowsPerPage(newPageSize);
  };

  if (user?.role !== 'ADMIN') {
    return <Redirect to="/404" />;
  }
  if (customers.loading) {
    return (
      <div
        style={{
          left: '50%',
          top: '50%',
          transform: 'translateY(-50%)',
          position: 'absolute',
        }}
      >
        <CircularProgress />
      </div>
    );
  }

  const handleEditClick = (id) => (event) => {
    event.stopPropagation();
    dispatch(setCurrentId(id));
  };

  // const handleDeleteClick = (id) => (event) => {
  //   event.stopPropagation();
  //   setSelectedId(id);
  //   setOpen(true);
  // };

  // const handleClose = () => {
  //   setSelectedId('');
  //   setOpen(false);
  // };

  // const handleAgree = (id) => {
  //   dispatch(deleteCustomer.deleteCustomerRequest(id));
  //   setSelectedId('');
  //   setOpen(false);
  // };

  const columns = [
    {
      field: 'avatar',
      disableExport: true,
      headerName: '#',
      width: 65,
      filterable: false,
      renderCell: (params) => (
        <img
          src={
            params.value ||
            'https://images.unsplash.com/photo-1579353977828-2a4eab540b9a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1974&q=80'
          }
          alt="img"
          style={{ width: '40px' }}
        />
      ),
    },
    { field: 'fullName', headerName: 'Tên', minWidth: 180, flex: 1 },
    { field: 'email', headerName: 'Email', minWidth: 200, flex: 1 },
    { field: 'phone', headerName: 'Số điện thoại', minWidth: 100, flex: 1 },
    { field: 'address', headerName: 'Địa chỉ', minWidth: 250, flex: 1 },
    {
      field: 'isBlock',
      headerName: 'Kích hoạt',
      minWidth: 80,
      flex: 1,
      renderCell: (param) =>
        param?.value ? (
          <RemoveCircleIcon color="error" />
        ) : (
          <CheckCircleOutlineIcon color="success" />
        ),
    },
    {
      field: 'actions',
      type: 'actions',
      headerName: 'Thao tác',
      minWidth: 80,
      flex: 1,
      cellClassName: 'actions',
      getActions: ({ id }) => [
        <GridActionsCellItem
          icon={<EditIcon color="warning" />}
          label="Edit"
          className="textPrimary"
          onClick={handleEditClick(id)}
          color="inherit"
        />,
        // <GridActionsCellItem
        //   icon={<DeleteForeverRoundedIcon color="error" />}
        //   label="Delete"
        //   onClick={handleDeleteClick(id)}
        //   color="inherit"
        // />,
      ],
    },
  ];

  return (
    <>
      {/* <DeleteButton
        open={open}
        id={selectedId}
        handleClose={handleClose}
        handleAgree={handleAgree}
      /> */}
      <DataTable
        component={AddModal}
        toast={toast}
        data={customers.data}
        columns={columns}
        rowsPerPage={rowsPerPage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        setShowModal={setShowModal}
      />
    </>
  );
};

export default AdminCustomers;
