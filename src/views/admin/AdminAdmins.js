import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import EditIcon from '@mui/icons-material/Edit';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import { CircularProgress } from '@mui/material';
import { GridActionsCellItem } from '@mui/x-data-grid';
import { useCallback, useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import AddModal from '../../components/admins/AddModal';
import DataTable from '../../components/layouts/DataTable';
import { AuthContext } from '../../contexts/AuthContext';
import { setCurrentId, showModal } from '../../redux/actions';
import { getAdmins } from '../../redux/actions/admins';
import { admins$, toast$ } from '../../redux/selectors';

const AdminAdmins = () => {
  const dispatch = useDispatch();
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const toast = useSelector(toast$);
  const admins = useSelector(admins$);
  const {
    authState: { user },
  } = useContext(AuthContext);

  useEffect(() => {
    if (admins.data.length === 0) {
      dispatch(getAdmins.getAdminsRequest('admin'));
    }
  }, [admins.data.length, dispatch]);

  const setShowModal = useCallback(() => {
    dispatch(showModal());
  }, [dispatch]);

  const handleChangeRowsPerPage = (newPageSize) => {
    setRowsPerPage(newPageSize);
  };

  if (user?.role !== 'ADMIN') {
    return <Redirect to="/404" />;
  }
  if (admins.loading) {
    return (
      <div
        style={{
          left: '50%',
          top: '50%',
          transform: 'translateY(-50%)',
          position: 'absolute',
        }}
      >
        <CircularProgress />
      </div>
    );
  }

  const handleEditClick = (id) => (event) => {
    event.stopPropagation();
    dispatch(setCurrentId(id));
  };

  const columns = [
    {
      field: 'avatar',
      disableExport: true,
      headerName: '#',
      width: 65,
      filterable: false,
      renderCell: (params) => (
        <img
          src={
            params.value ||
            'https://images.unsplash.com/photo-1579353977828-2a4eab540b9a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1974&q=80'
          }
          alt="img"
          style={{ width: '40px' }}
        />
      ),
    },
    { field: 'fullName', headerName: 'Tên', minWidth: 180, flex: 1 },
    { field: 'email', headerName: 'Email', minWidth: 200, flex: 1 },
    { field: 'phone', headerName: 'Số điện thoại', minWidth: 100, flex: 1 },
    { field: 'address', headerName: 'Địa chỉ', minWidth: 250, flex: 1 },
    {
      field: 'isBlock',
      headerName: 'Kích hoạt',
      minWidth: 80,
      flex: 1,
      renderCell: (param) =>
        param?.value ? (
          <RemoveCircleIcon color="error" />
        ) : (
          <CheckCircleOutlineIcon color="success" />
        ),
    },
    {
      field: 'actions',
      type: 'actions',
      headerName: 'Thao tác',
      minWidth: 80,
      flex: 1,
      cellClassName: 'actions',
      getActions: ({ id }) => [
        <GridActionsCellItem
          icon={<EditIcon color="warning" />}
          label="Edit"
          className="textPrimary"
          onClick={handleEditClick(id)}
          color="inherit"
        />,
      ],
    },
  ];

  return (
    <DataTable
      component={AddModal}
      toast={toast}
      data={admins.data}
      columns={columns}
      rowsPerPage={rowsPerPage}
      handleChangeRowsPerPage={handleChangeRowsPerPage}
      setShowModal={setShowModal}
    />
  );
};

export default AdminAdmins;
