import { CircularProgress, Container, Grid } from '@mui/material';
import moment from 'moment';
import { useCallback, useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import DataTable from '../../components/layouts/DataTable';
import AddModal from '../../components/orders/AddModal';
import ComposedChartRevenue from '../../components/revenue/ComposedChartRevenue';
import PieChartRevenue from '../../components/revenue/PieChartRevenue';
import { AuthContext } from '../../contexts/AuthContext';
import { showModal } from '../../redux/actions';
import { getOrders } from '../../redux/actions/orders';
import { orders$, toast$ } from '../../redux/selectors';

const Revenue = () => {
  const dispatch = useDispatch();
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [chartData, setChartData] = useState([]);
  const [pieData, setPieData] = useState([]);
  const [totalRevenue, setTotalRevenue] = useState(0);
  const toast = useSelector(toast$);
  const orders = useSelector(orders$);
  const {
    authState: { user },
  } = useContext(AuthContext);

  useEffect(() => {
    if (orders.data.length === 0) {
      dispatch(getOrders.getOrdersRequest());
    }
  }, [dispatch, orders.data.length]);

  const setShowModal = useCallback(() => {
    dispatch(showModal());
  }, [dispatch]);

  useEffect(() => {
    let newChartData = [];
    let newPieData = [];
    let total = 0;
    orders?.data?.forEach((item) => {
      // chartData
      if (newChartData.length > 0) {
        if (
          moment(item.createdAt)
            .format('L')
            .includes(newChartData[newChartData.length - 1]?.name)
        ) {
          newChartData[newChartData.length - 1].value =
            newChartData[newChartData.length - 1].value + item.totalPrice;
        } else {
          newChartData.push({
            id: item.createdAt,
            name: moment(item.createdAt).format('L'),
            value: item.totalPrice,
          });
        }
      } else {
        newChartData.push({
          id: item.createdAt,
          name: moment(item.createdAt).format('L'),
          value: item.totalPrice,
        });
      }
      total = total + item.totalPrice;

      // pieData
      item?.orderDetail.forEach((orderDetailItem) => {
        if (newPieData.length > 0) {
          let i = -1;
          const newPieDataExist = newPieData?.find((newPieDataItem) => {
            i++;
            return newPieDataItem.name.includes(
              orderDetailItem.product.category.name
            );
          });

          if (newPieDataExist) {
            newPieData[i].value = newPieData[i].value + orderDetailItem.price;
          } else {
            newPieData.push({
              id: orderDetailItem.product.category.name,
              name: orderDetailItem.product.category.name,
              value: orderDetailItem.price,
            });
          }
        } else {
          newPieData.push({
            id: orderDetailItem.product.category.name,
            name: orderDetailItem.product.category.name,
            value: orderDetailItem.price,
          });
        }
      });
    });
    setTotalRevenue(total);
    setChartData(newChartData);
    setPieData(newPieData);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [orders.data]);

  if (user?.role !== 'ADMIN') {
    return <Redirect to="/404" />;
  }
  if (orders.loading) {
    return (
      <div
        style={{
          left: '50%',
          top: '50%',
          transform: 'translateY(-50%)',
          position: 'absolute',
        }}
      >
        <CircularProgress />
      </div>
    );
  }

  const handleChangeRowsPerPage = (newPageSize) => {
    setRowsPerPage(newPageSize);
  };

  const columns = [
    {
      field: 'name',
      headerName: 'Ngày',
      type: 'date',
      width: 60,
      flex: 1,
    },
    {
      field: 'value',
      headerName: 'Tổng tiền',
      width: 100,
      flex: 1,
      valueGetter: (param) => {
        return `${String(param.value).replace(/(.)(?=(\d{3})+$)/g, '$1,')} ₫`;
      },
    },
  ];

  return (
    <>
      <Container sx={{ pt: 4, mb: '-150px' }}>
        <Grid container spacing={1} style={{ height: '500px' }}>
          <Grid item xs={12} sm={7} md={7}>
            <ComposedChartRevenue data={chartData} />
          </Grid>
          <Grid item sm={5} md={5}>
            <PieChartRevenue data={pieData} />
          </Grid>
        </Grid>
      </Container>
      <strong style={{ marginLeft: '30px' }}>
        Tổng doanh thu:{' '}
        {String(totalRevenue).replace(/(.)(?=(\d{3})+$)/g, '$1,')} ₫
      </strong>
      <DataTable
        component={AddModal}
        toast={toast}
        data={chartData}
        columns={columns}
        rowsPerPage={rowsPerPage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        setShowModal={setShowModal}
      />
    </>
  );
};

export default Revenue;
