/* eslint-disable eqeqeq */
/* eslint-disable react-hooks/exhaustive-deps */
import DeleteForeverRoundedIcon from '@mui/icons-material/DeleteForeverRounded';
import EditIcon from '@mui/icons-material/Edit';
import { CircularProgress } from '@mui/material';
import { GridActionsCellItem } from '@mui/x-data-grid';
import { useCallback, useEffect, useState, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import DataTable from '../../components/layouts/DataTable';
import DeleteButton from '../../components/layouts/DeleteButton';
import AddModal from '../../components/products/AddModal';
import { setCurrentId, showModal } from '../../redux/actions';
import { deleteProduct, getProducts } from '../../redux/actions/products';
import { products$, toast$ } from '../../redux/selectors';
import { AuthContext } from '../../contexts/AuthContext';
import moment from 'moment';
import 'moment/locale/vi';
moment.locale('vi');

const AdminProducts = () => {
  const dispatch = useDispatch();
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const toast = useSelector(toast$);
  const products = useSelector(products$);
  const [open, setOpen] = useState(false);
  const [selectedId, setSelectedId] = useState('');
  const {
    authState: { user },
  } = useContext(AuthContext);

  useEffect(() => {
    if (products.data.length === 0) {
      dispatch(getProducts.getProductsRequest());
    }
  }, [dispatch, products.data.length]);

  const setShowModal = useCallback(() => {
    dispatch(showModal());
  }, [dispatch]);

  const handleChangeRowsPerPage = (newPageSize) => {
    setRowsPerPage(newPageSize);
  };

  if (user?.role !== 'ADMIN') {
    return <Redirect to="/404" />;
  }
  if (products.loading) {
    return (
      <div
        style={{
          left: '50%',
          top: '50%',
          transform: 'translateY(-50%)',
          position: 'absolute',
        }}
      >
        <CircularProgress />
      </div>
    );
  }

  const handleEditClick = (id) => (event) => {
    event.stopPropagation();
    dispatch(setCurrentId(id));
  };

  const handleDeleteClick = (id) => (event) => {
    event.stopPropagation();
    setSelectedId(id);
    setOpen(true);
  };

  const handleClose = () => {
    setSelectedId('');
    setOpen(false);
  };

  const handleAgree = (id) => {
    dispatch(deleteProduct.deleteProductRequest(id));
    setSelectedId('');
    setOpen(false);
  };

  const columns = [
    {
      field: 'image',
      disableExport: true,
      headerName: '#',
      width: 75,
      filterable: false,
      renderCell: (params) => (
        <img src={params.value} alt="img" style={{ width: '50px' }} />
      ),
    },
    {
      field: 'name',
      headerName: 'Tên sản phẩm',
      minWidth: 120,
      flex: 1,
      renderCell: (params) => (
        <Link to={`products/${params.id}`}>{params.value}</Link>
      ),
    },
    {
      field: 'description',
      headerName: 'Mô tả',
      minWidth: 100,
      flex: 1,
    },
    {
      field: 'price',
      headerName: 'Giá',
      minWidth: 70,
      flex: 1,
    },
    {
      field: 'quantity',
      headerName: 'Số lượng',
      minWidth: 70,
      flex: 1,
    },
    {
      field: 'unit',
      headerName: 'Đơn vị',
      minWidth: 70,
      flex: 1,
    },
    {
      field: 'discount',
      headerName: 'Giảm giá',
      minWidth: 70,
      flex: 1,
    },
    {
      field: 'category',
      headerName: 'Loại sản phẩm',
      minWidth: 70,
      flex: 1,
      valueGetter: (param) => {
        return `${param.value.name}`;
      },
    },
    {
      field: 'createdAt',
      headerName: 'Ngày tạo',
      type: 'date',
      minWidth: 70,
      flex: 1,
      valueGetter: (param) => {
        return `${moment(param.value).format('l')}`;
      },
    },
    {
      field: 'actions',
      type: 'actions',
      headerName: 'Thao tác',
      minWidth: 70,
      flex: 1,
      cellClassName: 'actions',
      getActions: ({ id }) => [
        <GridActionsCellItem
          icon={<EditIcon color="warning" />}
          label="Edit"
          className="textPrimary"
          onClick={handleEditClick(id)}
          color="inherit"
        />,
        <GridActionsCellItem
          icon={<DeleteForeverRoundedIcon color="error" />}
          label="Delete"
          onClick={handleDeleteClick(id)}
          color="inherit"
        />,
      ],
    },
  ];

  return (
    <>
      <DeleteButton
        open={open}
        id={selectedId}
        handleClose={handleClose}
        handleAgree={handleAgree}
      />
      <DataTable
        component={AddModal}
        toast={toast}
        data={products.data}
        columns={columns}
        rowsPerPage={rowsPerPage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        setShowModal={setShowModal}
      />
    </>
  );
};

export default AdminProducts;
