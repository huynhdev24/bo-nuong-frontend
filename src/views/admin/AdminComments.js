import DeleteForeverRoundedIcon from '@mui/icons-material/DeleteForeverRounded';
import EditIcon from '@mui/icons-material/Edit';
import { CircularProgress, Rating } from '@mui/material';
import { GridActionsCellItem } from '@mui/x-data-grid';
import { useCallback, useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import AddModal from '../../components/comments/AddModal';
import DataTable from '../../components/layouts/DataTable';
import DeleteButton from '../../components/layouts/DeleteButton';
import { AuthContext } from '../../contexts/AuthContext';
import { setCurrentId, showModal } from '../../redux/actions';
import { deleteComment, getAllComments } from '../../redux/actions/comments';
import { comments$, toast$ } from '../../redux/selectors';
import moment from 'moment';
import 'moment/locale/vi';
moment.locale('vi');

const AdminComments = () => {
  const dispatch = useDispatch();
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const toast = useSelector(toast$);
  const comments = useSelector(comments$);
  const [open, setOpen] = useState(false);
  const [selectedId, setSelectedId] = useState('');
  const {
    authState: { user },
  } = useContext(AuthContext);

  useEffect(() => {
    if (comments.data.length === 0)
      dispatch(getAllComments.getAllCommentsRequest());
  }, [comments.data.length, dispatch]);

  const setShowModal = useCallback(() => {
    dispatch(showModal());
  }, [dispatch]);

  const handleChangeRowsPerPage = (newPageSize) => {
    setRowsPerPage(newPageSize);
  };

  if (user?.role !== 'ADMIN') {
    return <Redirect to="/404" />;
  }
  if (comments.loading) {
    return (
      <div
        style={{
          left: '50%',
          top: '50%',
          transform: 'translateY(-50%)',
          position: 'absolute',
        }}
      >
        <CircularProgress />
      </div>
    );
  }

  const handleEditClick = (id) => (event) => {
    event.stopPropagation();
    dispatch(setCurrentId(id));
  };

  const handleDeleteClick = (id) => (event) => {
    event.stopPropagation();
    setSelectedId(id);
    setOpen(true);
  };

  const handleClose = () => {
    setSelectedId('');
    setOpen(false);
  };

  const handleAgree = (id) => {
    dispatch(deleteComment.deleteCommentRequest(id));
    setSelectedId('');
    setOpen(false);
  };

  const columns = [
    {
      field: 'product',
      headerName: 'Sản phẩm',
      minWidth: 150,
      flex: 1,
      valueGetter: (param) => {
        return `${param.value.name}`;
      },
    },
    {
      field: 'user',
      headerName: 'Người đăng',
      minWidth: 150,
      flex: 1,
      valueGetter: (param) => {
        return `${param.value.fullName}`;
      },
    },
    { field: 'content', headerName: 'Nội dung', minWidth: 250, flex: 1 },
    {
      field: 'rating',
      headerName: 'Đánh giá',
      minWidth: 150,
      flex: 1,
      renderCell: (param) => (
        <Rating name="read-only" value={param.value || 5} readOnly />
      ),
    },
    {
      field: 'createdAt',
      headerName: 'Ngày tạo',
      type: 'date',
      minWidth: 70,
      flex: 1,
      valueGetter: (param) => {
        return `${moment(param.value).format('l')}`;
      },
    },
    {
      field: 'actions',
      type: 'actions',
      headerName: 'Thao tác',
      minWidth: 100,
      flex: 1,
      cellClassName: 'actions',
      getActions: ({ id }) => [
        <GridActionsCellItem
          icon={<EditIcon color="warning" />}
          label="Edit"
          className="textPrimary"
          onClick={handleEditClick(id)}
          color="inherit"
        />,
        <GridActionsCellItem
          icon={<DeleteForeverRoundedIcon color="error" />}
          label="Delete"
          onClick={handleDeleteClick(id)}
          color="inherit"
        />,
      ],
    },
  ];

  return (
    <>
      <DeleteButton
        open={open}
        id={selectedId}
        handleClose={handleClose}
        handleAgree={handleAgree}
      />
      <DataTable
        component={AddModal}
        toast={toast}
        data={comments.data}
        columns={columns}
        rowsPerPage={rowsPerPage}
        handleChangeRowsPerPage={handleChangeRowsPerPage}
        setShowModal={setShowModal}
      />
    </>
  );
};

export default AdminComments;
