import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Typography,
} from '@mui/material';
import axios from 'axios';
import queryString from 'query-string';
import { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { apiURL } from '../../api';
import Copyright from '../../components/layouts/Copyright';

const PaymentFailed = () => {
  const location = useLocation();
  const { token, order_id } = queryString.parse(location.search);
  const [success, setSuccess] = useState('');
  const [message, setMessage] = useState('');

  useEffect(() => {
    if (token !== undefined || order_id !== undefined) {
      axios
        .put(`${apiURL}/orders/update-checkout`, { token, id: order_id })
        .then((response) => {
          setSuccess(response.data.success);
          setMessage(response.data.message);
        })
        .catch((error) => {
          console.log(error);
          setSuccess(error.response.data.success);
          setMessage(error.response.data.message);
        });
    }
  }, [token, order_id]);

  let body;
  if (token === undefined || order_id === undefined) {
    body = (
      <CardContent>
        <Typography gutterBottom variant="h5" component="div" align="center">
          Thanh toán thất bại
        </Typography>
        <Typography variant="body2" color="text.secondary" align="center">
          Vui lòng thử lại
        </Typography>
        <CardActions>
          <Button type="submit" fullWidth variant="contained" sx={{ mt: 3 }}>
            <Link className="text-white" to="/checkout">
              Đến trang thanh toán
            </Link>
          </Button>
        </CardActions>
      </CardContent>
    );
  } else {
    body = (
      <>
        {success ? (
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              {message}
            </Typography>
            <Typography variant="body2" color="text.secondary" align="center">
              Cảm ơn bạn đã đặt hàng, chúc bạn ngon miệng <br />
              Vui lòng chờ, chúng tôi sẽ giao hàng ngay
            </Typography>
            <CardActions>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3 }}
              >
                <a href="/products" style={{ color: 'white' }}>
                  Tiếp tục mua sắm
                </a>
              </Button>
            </CardActions>
          </CardContent>
        ) : (
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              {message}
            </Typography>
            <Typography variant="body2" color="text.secondary" align="center">
              Đã xảy ra lỗi. <br />
              Vui lòng thử lại!
            </Typography>
            <CardActions>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3 }}
              >
                <Link to="/checkout" style={{ color: 'white' }}>
                  Đến trang thanh toán
                </Link>
              </Button>
            </CardActions>
          </CardContent>
        )}
      </>
    );
  }

  return (
    <>
      <Typography component="h1" variant="h4">
        Kết quả thanh toán
      </Typography>
      <Box component="form" sx={{ mt: 1 }}>
        <Card sx={{ minWidth: 275 }}>{body}</Card>
        <Copyright sx={{ mt: 5 }} />
      </Box>
    </>
  );
};

export default PaymentFailed;
