import { Button, Card, CardContent, Paper, Typography } from '@mui/material';
import Image from '../../assets/banner.jpeg';

const PaymentFailed = () => {
  return (
    <Paper
      sx={{
        position: 'absolute',
        width: '-webkit-fill-available',
        height: '100vh',
        borderRadius: 0,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundImage: `url(${Image})`,
      }}
    >
      <Card
        sx={{
          maxWidth: 500,
          textAlign: 'center',
          margin: 'auto',
          boxShadow: '0px 5px 14px rgb(100 116 139)',
          top: '50%',
          transform: 'translateY(50%)',
          borderRadius: 4,
        }}
      >
        <CardContent>
          <Typography gutterBottom variant="h5" component="div" color="red">
            Thanh toán thất bại
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Vui lòng thực hiện lại
            <br />
            Xin hãy kiểm tra lại hoặc liên hệ
          </Typography>
          <Button
            href="https://www.facebook.com/B%C3%B2-N%C6%B0%E1%BB%9Bng-V%E1%BB%89a-H%C3%A8-107187518619137"
            target="_blank"
            color="error"
            variant="outlined"
            sx={{ m: 1 }}
          >
            Support@Nhóm 2 - 19DTHD4
          </Button>
          <br />
          <Button variant="outlined">
            <a href="/">Quay lại trang chủ</a>
          </Button>
        </CardContent>
      </Card>
    </Paper>
  );
};

export default PaymentFailed;
