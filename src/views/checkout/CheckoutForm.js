import { Button, Card, CardContent, Paper, Typography } from '@mui/material';
import { loadStripe } from '@stripe/stripe-js';
import axios from 'axios';
import { useContext } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { apiURL } from '../../api';
import Image from '../../assets/banner.jpeg';
import { AuthContext } from '../../contexts/AuthContext';
import { CartContext } from '../../contexts/CartContext';
import queryString from 'query-string';

const CheckoutForm = () => {
  const {
    cartState: { cart },
    deleteCart,
  } = useContext(CartContext);
  const {
    authState: { user, isAuthenticated, authLoading },
  } = useContext(AuthContext);
  const history = useHistory();
  const location = useLocation();
  const { note } = queryString.parse(location.search);

  if (!authLoading && !isAuthenticated) {
    history.push(`/login?RedirectTo=${location.pathname}${location.search}`);
  }

  const onSubmit = async (event) => {
    event.preventDefault();
    const orderDetail = [];
    cart.forEach((item) => {
      orderDetail.push({
        id: item.product.id,
        quantity: item.quantity,
      });
    });
    deleteCart();
    const stripe = await loadStripe(
      'pk_test_51KkIaFKMTZjXsvPFEDrEMfFhgdDH7PSGRmSemHIoKruZIrIxBpYWhNsMilLziAhZMF8MTX56VrTSsBydCPIN86hG00qKtamo4q'
    );
    axios
      .post(`${apiURL}/orders/create-checkout-session`, {
        userId: user.id,
        orderDetail,
        note: note ?? '',
      })
      .then((response) => {
        stripe.redirectToCheckout({
          sessionId: response.data?.sessionId,
        });
      })
      .catch((err) => console.log(err));
  };

  return (
    <Paper
      sx={{
        position: 'absolute',
        width: '-webkit-fill-available',
        height: '100vh',
        borderRadius: 0,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundImage: `url(${Image})`,
      }}
    >
      <Card
        sx={{
          maxWidth: 500,
          textAlign: 'center',
          margin: 'auto',
          boxShadow: '0px 5px 14px rgb(100 116 139)',
          top: '50%',
          transform: 'translateY(50%)',
          borderRadius: 4,
        }}
      >
        <CardContent>
          <Typography gutterBottom variant="h5" component="div" color="red">
            Đặt hàng thành công
          </Typography>
          <Button variant="outlined" onClick={onSubmit}>
            Đến trang thanh toán
          </Button>
        </CardContent>
      </Card>
    </Paper>
  );
};

export default CheckoutForm;
