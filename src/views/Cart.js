import { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { CartContext } from "../contexts/CartContext";

const Cart = () => {
  const {
    cartState: { cart },
    updateCart,
    deleteCartItem,
    deleteCart,
  } = useContext(CartContext);
  const [total, setTotal] = useState(0);
  const [discount, setDiscount] = useState(0);

  const onDeleteCartItem = (product) => {
    deleteCartItem(product);
  };

  const onDeleteCart = () => {
    deleteCart();
    setTotal(0);
    setDiscount(0);
  };

  useEffect(() => {
    if (cart) {
      let sumTotal = 0;
      let sumDiscount = 0;
      cart.forEach((item) => {
        sumTotal += item.total;
        sumDiscount += item.product.discount;
      });
      setTotal(sumTotal);
      setDiscount(sumDiscount);
    }
  }, [cart]);

  const onClickDec = (index, quantity) => {
    if (quantity > 1) {
      cart[index].quantity--;
    } else {
      cart[index].quantity = 1;
    }
    updateCart(index, cart[index].quantity);
  };

  const onClickInc = (index) => {
    cart[index].quantity++;
    updateCart(index, cart[index].quantity);
  };

  return (
    <>
      {/* <!-- Breadcrumb Section Begin --> */}
      <section
        className="breadcrumb-section set-bg"
        style={{
          backgroundImage: `url(img/breadcrumb.jpg)`,
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <div className="breadcrumb__text">
                <h2>Giỏ Hàng</h2>
                <div className="breadcrumb__option">
                  <a href="/">Trang Chủ</a>
                  <span>Chi Tiết Giỏ Hàng</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- Breadcrumb Section End -->

  <!-- Shoping Cart Section Begin --> */}
      <section className="shoping-cart spad">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="shoping__cart__table">
                <table>
                  <thead>
                    <tr>
                      <th className="shoping__product">Sản phẩm</th>
                      <th>Giá</th>
                      <th>Giảm giá</th>
                      <th>Số lượng</th>
                      <th>Tổng</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {cart &&
                      cart.map((item, index) => (
                        <tr key={item?.product?.id}>
                          <td className="shoping__cart__item">
                            <img
                              src={item?.product?.image}
                              alt=""
                              width="150"
                            />
                            <h5>{item?.product?.name}</h5>
                          </td>
                          <td className="shoping__cart__price">
                            {String(item?.product?.price).replace(
                              /(.)(?=(\d{3})+$)/g,
                              "$1,"
                            )}{" "}
                            &#8363;
                          </td>
                          <td className="shoping__cart__price">
                            {String(item?.product?.discount).replace(
                              /(.)(?=(\d{3})+$)/g,
                              "$1,"
                            )}{" "}
                            &#8363;
                          </td>
                          <td className="shoping__cart__quantity">
                            <div className="quantity">
                              <div className="pro-qty">
                                <span
                                  className="dec qtybtn"
                                  onClick={() =>
                                    onClickDec(index, item?.quantity)
                                  }
                                >
                                  -
                                </span>
                                <input
                                  type="text"
                                  value={item?.quantity}
                                  readOnly
                                />
                                <span
                                  className="inc qtybtn"
                                  onClick={() => onClickInc(index)}
                                >
                                  +
                                </span>
                              </div>
                            </div>
                          </td>
                          <td className="shoping__cart__total">
                            {String(item?.total).replace(
                              /(.)(?=(\d{3})+$)/g,
                              "$1,"
                            )}{" "}
                            &#8363;
                          </td>
                          <td className="shoping__cart__item__close">
                            <span
                              className="icon_close"
                              onClick={() => onDeleteCartItem(item?.product)}
                            ></span>
                          </td>
                        </tr>
                      ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="shoping__cart__btns">
                <a href="/" className="primary-btn cart-btn">
                  TIẾP TỤC MUA SẮM
                </a>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a
                  onClick={onDeleteCart}
                  className="primary-btn cart-btn cart-btn-right"
                >
                  <span className="icon_loading"></span>
                  XÓA GIỎ HÀNG
                </a>
              </div>
            </div>
            <div className="col-lg-6">
              {/* <div className="shoping__continue">
                <div className="shoping__discount">
                  <h5>Mã giảm giá</h5>
                  <form action="#">
                    <input type="text" placeholder="Nhập mã giảm giá" />
                    <button type="submit" className="site-btn">
                      ÁP DỤNG
                    </button>
                  </form>
                </div>
              </div> */}
            </div>
            <div className="col-lg-6">
              <div className="shoping__checkout">
                <h5>Tổng giá tiền</h5>
                <ul>
                  <li>
                    Đã giảm{" "}
                    <span>
                      {String(discount).replace(/(.)(?=(\d{3})+$)/g, "$1,")}{" "}
                      &#8363;
                    </span>
                  </li>
                  <li>
                    Thành tiền{" "}
                    <span>
                      {String(total).replace(/(.)(?=(\d{3})+$)/g, "$1,")}{" "}
                      &#8363;
                    </span>
                  </li>
                </ul>
                <Link to="/checkout" className="primary-btn">
                  ĐẶT HÀNG
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- Shoping Cart Section End --> */}
    </>
  );
};

export default Cart;
