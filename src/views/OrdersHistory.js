import VisibilityIcon from '@mui/icons-material/Visibility';
import { CircularProgress } from '@mui/material';
import { GridActionsCellItem } from '@mui/x-data-grid';
import moment from 'moment';
import 'moment/locale/vi';
import { useCallback, useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import DataTable from '../components/layouts/DataTable';
import AddModal from '../components/orders/AddModal';
import UserOderDetailModal from '../components/orders/UserOderDetailModal';
import { AuthContext } from '../contexts/AuthContext';
import { showModal } from '../redux/actions';
import { getUserOrders } from '../redux/actions/orders';
import { orders$, toast$ } from '../redux/selectors';
moment.locale('vi');

const OrdersHistory = () => {
  const history = useHistory();
  const location = useLocation();
  const dispatch = useDispatch();
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const toast = useSelector(toast$);
  const orders = useSelector(orders$);
  const [open, setOpen] = useState(false);
  const [selectedId, setSelectedId] = useState('');
  const {
    authState: { user, isAuthenticated, authLoading },
  } = useContext(AuthContext);

  if (!authLoading && !isAuthenticated) {
    history.push(`/login?RedirectTo=${location.pathname}${location.search}`);
  }

  useEffect(() => {
    if (orders.userOrders.length === 0) {
      dispatch(getUserOrders.getUserOrdersRequest(user?.id));
    }
  }, [dispatch, orders.userOrders.length, user?.id]);

  const setShowModal = useCallback(() => {
    dispatch(showModal());
  }, [dispatch]);

  const handleChangeRowsPerPage = (newPageSize) => {
    setRowsPerPage(newPageSize);
  };

  if (orders.loading) {
    return (
      <div
        style={{
          left: '50%',
          top: '50%',
          transform: 'translateY(-50%)',
          position: 'absolute',
        }}
      >
        <CircularProgress />
      </div>
    );
  }

  const handleClose = () => {
    setSelectedId('');
    setOpen(false);
  };

  const handleSeeDetailClick = (id) => (event) => {
    event.stopPropagation();
    setSelectedId(id);
    setOpen(true);
  };

  const columns = [
    { field: 'id', headerName: 'Mã đơn', width: 160, flex: 1 },
    {
      field: 'isDelivery',
      headerName: 'Trạng thái giao hàng',
      width: 80,
      flex: 1,
      valueGetter: (param) => {
        return param.value ? 'Đã giao' : 'Chưa giao';
      },
    },
    {
      field: 'isPayment',
      headerName: 'Trạng thái thanh toán',
      width: 80,
      flex: 1,
      valueGetter: (param) => {
        return param.value ? 'Đã thanh toán' : 'Chưa thanh toán';
      },
    },
    {
      field: 'totalPrice',
      headerName: 'Tổng tiền',
      width: 100,
      flex: 1,
      valueGetter: (param) => {
        return `${String(param.value).replace(/(.)(?=(\d{3})+$)/g, '$1,')} ₫`;
      },
    },
    {
      field: 'createdAt',
      headerName: 'Ngày tạo',
      type: 'date',
      width: 60,
      flex: 1,
      valueGetter: (param) => {
        return `${moment(param.value).format('l')}`;
      },
    },

    {
      field: 'actions',
      type: 'actions',
      headerName: 'Xem chi tiết',
      width: 60,
      flex: 1,
      cellClassName: 'actions',
      getActions: ({ id }) => [
        <GridActionsCellItem
          icon={<VisibilityIcon color="info" />}
          label="Watch"
          className="textPrimary"
          onClick={handleSeeDetailClick(id)}
          color="inherit"
        />,
      ],
    },
  ];

  return (
    <>
      {/* <!-- Breadcrumb Section Begin --> */}
      <section
        className="breadcrumb-section set-bg"
        style={{
          backgroundImage: `url(img/breadcrumb.jpg)`,
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <div className="breadcrumb__text">
                <h2>Lịch sử mua hàng</h2>
                <div className="breadcrumb__option">
                  <a href="/">Trang chủ</a>
                  <span>Lịch sử mua hàng</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- Breadcrumb Section End --> */}
      <UserOderDetailModal
        open={open}
        id={selectedId}
        handleClose={handleClose}
      />
      <section>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <DataTable
                component={AddModal}
                toast={toast}
                data={orders.userOrders}
                columns={columns}
                rowsPerPage={rowsPerPage}
                handleChangeRowsPerPage={handleChangeRowsPerPage}
                setShowModal={setShowModal}
              />
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default OrdersHistory;
