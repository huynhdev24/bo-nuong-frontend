import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
} from "@mui/material";
import { useContext, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import { AuthContext } from "../contexts/AuthContext";
import { CartContext } from "../contexts/CartContext";
import { showToast } from "../redux/actions";
import { createOrder } from "../redux/actions/orders";

const Checkout = () => {
  const {
    authState: { user, isAuthenticated, authLoading },
  } = useContext(AuthContext);
  const {
    cartState: { cart },
    deleteCart,
  } = useContext(CartContext);
  const [total, setTotal] = useState(0);
  const history = useHistory();
  const location = useLocation();
  const [value, setValue] = useState(true);
  const [note, setNote] = useState("");
  const dispatch = useDispatch();

  if (!authLoading && !isAuthenticated) {
    history.push(`/login?RedirectTo=${location.pathname}${location.search}`);
  }

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const handleChangeNote = (event) => {
    setNote(event.target.value);
  };

  useEffect(() => {
    if (cart) {
      let sumTotal = 0;
      cart.forEach((item) => {
        sumTotal += item.total;
      });
      setTotal(sumTotal);
    }
  }, [cart]);

  const onCheckout = (event) => {
    event.preventDefault();
    const orderDetail = [];
    cart.forEach((item) => {
      orderDetail.push({
        id: item.product.id,
        quantity: item.quantity,
      });
    });
    console.log(value);
    if (value === true) {
      history.push(`/checkout/payment?note=${note}`);
    } else {
      dispatch(
        createOrder.createOrderRequest({
          userId: user.id,
          orderDetail,
          note,
        })
      );
      deleteCart();
      dispatch(
        showToast({
          message: "Đã thêm vào đơn hàng",
          type: "success",
        })
      );
      history.push("/");
    }
  };

  return (
    <>
      <section
        className="breadcrumb-section set-bg"
        style={{
          backgroundImage: `url(img/breadcrumb.jpg)`,
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <div className="breadcrumb__text">
                <h2>Đặt hàng</h2>
                <div className="breadcrumb__option">
                  <a href="/">Trang Chủ</a>
                  <span>Xác Nhận Đặt Hàng</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="checkout spad">
        <div className="container">
          <div className="checkout__form">
            <h4>Chi Tiết Thanh Toán</h4>
            <form
              onSubmit={(event) => {
                event.preventDefault();
              }}
            >
              <div className="row">
                <div className="col-lg-6 col-md-6">
                  <div className="checkout__input">
                    <p>
                      Họ Tên<span>*</span>
                    </p>
                    <input type="text" defaultValue={user?.fullName} disabled />
                  </div>
                  <div className="checkout__input">
                    <p>
                      Địa chỉ<span>*</span>
                    </p>
                    <input type="text" defaultValue={user?.address} disabled />
                  </div>
                  <div className="row">
                    <div className="col-lg-4">
                      <div className="checkout__input">
                        <p>
                          Số điện thoại<span>*</span>
                        </p>
                        <input
                          type="text"
                          defaultValue={user?.phone}
                          disabled
                        />
                      </div>
                    </div>
                    <div className="col-lg-8">
                      <div className="checkout__input">
                        <p>
                          Email<span>*</span>
                        </p>
                        <input
                          type="text"
                          defaultValue={user?.email}
                          disabled
                        />
                      </div>
                    </div>
                  </div>
                  {/* <div className="checkout__input__checkbox">
                    <label htmlFor="acc">
                      Tạo tài khoản?
                      <input type="checkbox" id="acc" />
                      <span className="checkmark"></span>
                    </label>
                  </div>
                  <p>
                    Tạo tài khoản bằng cách nhập thông tin bên dưới. Nếu bạn đã
                    có tài khoản vui lòng đăng nhập ở đầu trang.
                  </p>
                  <div className="checkout__input">
                    <p>
                      Mật khẩu<span>*</span>
                    </p>
                    <input type="text" />
                  </div>
                  <div className="checkout__input__checkbox">
                    <label htmlFor="diff-acc">
                      Gửi đến địa chỉ khác?
                      <input type="checkbox" id="diff-acc" />
                      <span className="checkmark"></span>
                    </label>
                  </div> */}
                  <div className="checkout__input">
                    <p>Ghi chú</p>
                    <input
                      type="text"
                      name="note"
                      value={note}
                      onChange={handleChangeNote}
                      placeholder="Ghi chú về đơn đặt hàng, ví dụ: những lưu ý đặc biệt khi giao hàng."
                    />
                  </div>
                </div>
                <div className="col-lg-6 col-md-6">
                  <div className="checkout__order">
                    <h4>Đơn Hàng Của Bạn</h4>
                    <div className="checkout__order__products">
                      Sản phẩm <span>Thành tiền</span>
                    </div>
                    <ul>
                      {cart &&
                        cart.map((item) => (
                          <li key={item.product.id}>
                            {item.product.name}{" "}
                            <span>
                              {String(item.total).replace(
                                /(.)(?=(\d{3})+$)/g,
                                "$1,"
                              )}{" "}
                              &#8363;
                            </span>
                          </li>
                        ))}
                    </ul>
                    {/* <div className="checkout__order__subtotal">
                      Tổng cộng <span>$750.99</span>
                    </div> */}
                    <div className="checkout__order__total">
                      Tổng cộng{" "}
                      <span>
                        {String(total).replace(/(.)(?=(\d{3})+$)/g, "$1,")}{" "}
                        &#8363;
                      </span>
                    </div>
                    {/* <div className="checkout__input__checkbox">
                      <label htmlFor="acc-or">
                        Tạo tài khoản?
                        <input type="checkbox" id="acc-or" />
                        <span className="checkmark"></span>
                      </label>
                    </div> */}
                    <p>
                      Cảm ơn bạn đã lựa chọn sản phẩm của chúng tôi, chúc bạn
                      ngon miệng.
                    </p>
                    <FormControl>
                      <FormLabel id="demo-controlled-radio-buttons-group">
                        Chọn phương thức thanh toán
                      </FormLabel>
                      <RadioGroup
                        aria-labelledby="demo-controlled-radio-buttons-group"
                        name="controlled-radio-buttons-group"
                        value={value}
                        onChange={handleChange}
                      >
                        <FormControlLabel
                          value={true}
                          control={<Radio />}
                          label="Thanh toán online"
                        />
                        <FormControlLabel
                          value={false}
                          control={<Radio />}
                          label="Thanh toán khi nhận hàng"
                        />
                      </RadioGroup>
                    </FormControl>
                    <button onClick={onCheckout} className="site-btn">
                      XÁC NHẬN ĐƠN HÀNG
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </section>
    </>
  );
};

export default Checkout;
