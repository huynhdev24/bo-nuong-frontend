/* eslint-disable react-hooks/exhaustive-deps */
import { TextField } from '@mui/material';
import { Box } from '@mui/system';
import { useContext, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import AlertMessage from '../components/layouts/AlertMessage';
import Tooltip from '../components/layouts/Tooltip';
import { AuthContext } from '../contexts/AuthContext';
import { showToast } from '../redux/actions';
import { updatePersonal } from '../redux/actions/customers';
import { toast$ } from '../redux/selectors';

const PersonalInfo = () => {
  const {
    authState: { user, isAuthenticated, authLoading },
    loadUser,
  } = useContext(AuthContext);
  const history = useHistory();
  const location = useLocation();
  const dispatch = useDispatch();
  const toast = useSelector(toast$);
  const [alert, setAlert] = useState(null);
  const [newInfo, setNewInfo] = useState({
    fullName: '',
    phone: '',
    address: '',
    avatar: '',
  });
  const { fullName, phone, address, avatar } = newInfo;

  if (!authLoading && !isAuthenticated) {
    history.push(`/login?RedirectTo=${location.pathname}${location.search}`);
  }

  useEffect(async () => {
    clearData();
    await loadUser();
  }, [authLoading]);

  const clearData = () => {
    document.getElementById('uploadCaptureInputFile').value = '';
    setNewInfo({
      fullName: user?.fullName || '',
      phone: user?.phone || '',
      address: user?.address || '',
      avatar: user?.avatar || '',
    });
  };

  const toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  const handleFileChange = async (event) => {
    const base64image = await toBase64(event.target.files[0]);
    setNewInfo({ ...newInfo, avatar: base64image });
  };

  const onChangeData = (event) =>
    setNewInfo({ ...newInfo, [event.target.name]: event.target.value });

  const onSubmit = (event) => {
    event.preventDefault();
    if (
      fullName === user?.fullName &&
      phone === user?.phone &&
      address === user?.address &&
      (avatar === user?.avatar || avatar === '')
    ) {
      console.log('nothing change');
      return;
    }
    if (!fullName) {
      setAlert({
        type: 'warning',
        message: 'Họ tên không được bỏ trống',
      });
      setTimeout(() => setAlert(null), 5000);
      return;
    }
    setAlert(null);
    dispatch(
      updatePersonal.updatePersonalRequest({ id: user?.id, ...newInfo })
    );
    dispatch(
      showToast({
        message: 'Vui lòng chờ! Dữ liệu đang được cập nhật...',
        type: 'warning',
      })
    );
    clearData();
    setTimeout(() => {
      if (toast?.type === 'success') window.location.reload();
    }, 5000);
  };

  return (
    <>
      <Tooltip toast={toast} />
      <section
        className="breadcrumb-section set-bg"
        style={{
          backgroundImage: `url(img/breadcrumb.jpg)`,
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <div className="breadcrumb__text">
                <h2>Thông tin cá nhân</h2>
                <div className="breadcrumb__option">
                  <a href="/">Trang Chủ</a>
                  <span>Thông tin cá nhân</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="checkout">
        <div className="container">
          <div className="checkout__form">
            {alert && <AlertMessage info={alert} />}
            <form
              onSubmit={(event) => {
                event.preventDefault();
              }}
            >
              <div className="row">
                <div className="col-lg-8 col-md-8">
                  <div className="checkout__input">
                    <p>
                      Họ Tên<span>*</span>
                    </p>
                    <input
                      type="text"
                      value={fullName}
                      name="fullName"
                      onChange={onChangeData}
                    />
                  </div>
                  <div className="checkout__input">
                    <p>
                      Địa chỉ<span>*</span>
                    </p>
                    <input
                      type="text"
                      value={address}
                      name="address"
                      onChange={onChangeData}
                    />
                  </div>
                  <div className="checkout__input">
                    <p>
                      Số điện thoại<span>*</span>
                    </p>
                    <input
                      type="text"
                      value={phone}
                      name="phone"
                      onChange={onChangeData}
                    />
                  </div>
                  <div className="checkout__input">
                    <p>
                      Email<span>*</span>
                    </p>
                    <input type="text" defaultValue={user?.email} disabled />
                  </div>
                </div>
                <div className="col-lg-4 col-md-4">
                  <div className="checkout__order">
                    <Box>
                      <img
                        alt="avatar"
                        src={
                          avatar ||
                          'https://i1.wp.com/www.polar-pinguin.berlin/wp-content/uploads/2017/12/image_preview.png?w=1080&ssl=1'
                        }
                      ></img>
                    </Box>
                    <TextField
                      id="uploadCaptureInputFile"
                      margin="dense"
                      type="file"
                      accept="image/*"
                      multiple={false}
                      fullWidth
                      variant="standard"
                      label="Ảnh"
                      helperText="Hãy chọn một bức ảnh thật đẹp"
                      name="avatar"
                      onChange={handleFileChange}
                    />
                    <button
                      onClick={clearData}
                      className="site-btn"
                      style={{ background: '#94998d' }}
                    >
                      Hủy thay đổi
                    </button>
                    <button onClick={onSubmit} className="site-btn">
                      Sửa thông tin
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </section>
    </>
  );
};

export default PersonalInfo;
