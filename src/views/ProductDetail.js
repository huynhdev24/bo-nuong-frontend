import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getProductComments } from '../redux/actions/comments';
import Comments from '../components/productDetail/Comments';
import Detail from '../components/productDetail/Detail';
import Header from '../components/productDetail/Header';
import Related from '../components/productDetail/Related';
import {
  getProductDetail,
  getRelatedProducts,
} from '../redux/actions/products';
import { comments$, products$ } from '../redux/selectors';

const ProductDetail = () => {
  const { id } = useParams();
  const dispatch = useDispatch(getProductDetail);
  const products = useSelector(products$);
  const comments = useSelector(comments$);

  useEffect(() => {
    dispatch(getProductDetail.getProductDetailRequest(id));
    dispatch(
      getRelatedProducts.getRelatedProductsRequest(
        products?.singleProduct?.category?.id
      )
    );
    dispatch(getProductComments.getProductCommentsRequest(id));
  }, [dispatch, id, products?.singleProduct?.category?.id]);

  return (
    <>
      {/* <!-- Breadcrumb Section Begin --> */}
      <Header products={products} />
      {/* <!-- Breadcrumb Section End --> */}

      {/* <!-- Product Details Section Begin --> */}
      <section className="product-details spad">
        <div className="container">
          <div className="row">
            <Detail products={products} comments={comments} />
            <div className="col-lg-12">
              <div className="product__details__tab">
                <ul className="nav nav-tabs" role="tablist">
                  <li className="nav-item">
                    <a
                      className="nav-link active"
                      data-toggle="tab"
                      href="#tabs-3"
                      role="tab"
                      aria-selected="true"
                    >
                      Bình luận <span>({comments.data.length})</span>
                    </a>
                  </li>
                </ul>
                <Comments comments={comments} productId={id} />
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- Product Details Section End --> */}

      {/* <!-- Related Product Section Begin --> */}
      <Related products={products} />
      {/* <!-- Related Product Section End --> */}
    </>
  );
};

export default ProductDetail;
