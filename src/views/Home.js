import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  getProducts,
  getLatestProducts,
  getTopRatingProducts,
  getDiscountProducts,
} from '../redux/actions/products';
import { products$ } from '../redux/selectors';
import Banner from '../components/home/Banner';
import Categories from '../components/home/Categories';
import Featured from '../components/home/Featured';
import Hero from '../components/home/Hero';
import LatestProduct from '../components/home/LatestProduct';
import MessengerCustomerChat from 'react-messenger-customer-chat';

const Home = () => {
  const dispatch = useDispatch();
  const products = useSelector(products$);

  useEffect(() => {
    if (products.data.length === 0) {
      dispatch(getProducts.getProductsRequest());
    }
    if (products.latestProducts.length === 0) {
      dispatch(getLatestProducts.getLatestProductsRequest());
    }
    if (products.topRatingProducts.length === 0) {
      dispatch(getTopRatingProducts.getTopRatingProductsRequest());
    }
    if (products.discountProducts.length === 0) {
      dispatch(getDiscountProducts.getDiscountProductsRequest());
    }
  }, [
    dispatch,
    products.data.length,
    products.discountProducts.length,
    products.latestProducts.length,
    products.topRatingProducts.length,
  ]);

  return (
    <>
      {/*<!-- Hero Section Begin --> */}
      <Hero />
      {/* <!-- Hero Section End -->
      <!-- Categories Section Begin --> */}
      <Categories />
      {/* <!-- Categories Section End -->
      <!-- Featured Section Begin --> */}
      <Featured products={products} />
      {/* <!-- Featured Section End -->
      <!-- Banner Begin --> */}
      <Banner />
      {/* <!-- Banner End -->
      <!-- Latest Product Section Begin --> */}
      <LatestProduct products={products} />
      {/* <!-- Latest Product Section End -->*/}

      <MessengerCustomerChat
        pageId="107187518619137"
        appId="3121594284755621"
      />
    </>
  );
};

export default Home;
