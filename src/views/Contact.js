import axios from 'axios';
import { useState } from 'react';
import { apiURL } from '../api';
import AlertMessage from '../components/layouts/AlertMessage';

const Contact = () => {
  const [alert, setAlert] = useState(null);
  const [formData, setFormData] = useState({
    fullName: '',
    email: '',
    phone: '',
    content: '',
  });
  const { fullName, email, phone, content } = formData;

  const onChangeFormData = (event) =>
    setFormData({ ...formData, [event.target.name]: event.target.value });

  const sendMail = async (formData) => {
    try {
      const response = await axios.post(`${apiURL}/auth/contact`, formData);
      return response.data;
    } catch (error) {
      console.log(error.response);
      if (error.response.data) return error.response.data;
      return { success: false, message: error.message };
    }
  };

  const onSubmitFormData = async (event) => {
    event.preventDefault();
    try {
      const responseData = await sendMail(formData);
      if (!responseData.success) {
        setAlert({ type: 'error', message: responseData.message });
        setTimeout(() => setAlert(null), 3000);
      } else {
        setAlert({ type: 'success', message: responseData.message });
        setTimeout(() => {
          setAlert(null);
        }, 3000);
      }
    } catch (error) {
      console.log(error);
    }
    setFormData({
      fullName: '',
      email: '',
      phone: '',
      content: '',
    });
  };

  return (
    <>
      {/* <!-- Breadcrumb Section Begin --> */}
      <section
        className="breadcrumb-section set-bg"
        style={{
          backgroundImage: `url(img/breadcrumb.jpg)`,
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12 text-center">
              <div className="breadcrumb__text">
                <h2>Liên hệ với chúng tôi</h2>
                <div className="breadcrumb__option">
                  <a href="/">Trang chủ</a>
                  <span>Liên hệ</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- Breadcrumb Section End -->

    <!-- Contact Section Begin --> */}
      <section className="contact spad">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-3 col-sm-6 text-center">
              <div className="contact__widget">
                <span className="icon_phone"></span>
                <h4>Điện thoại</h4>
                <p>0365.204.529</p>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6 text-center">
              <div className="contact__widget">
                <span className="icon_pin_alt"></span>
                <h4>Địa chỉ</h4>
                <p>11 Đường số 85, Phường Hiệp Phú, Quận 9, TP.HCM</p>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6 text-center">
              <div className="contact__widget">
                <span className="icon_clock_alt"></span>
                <h4>Giờ mở cửa</h4>
                <p>10:00 am - 23:00 pm</p>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-sm-6 text-center">
              <div className="contact__widget">
                <span className="icon_mail_alt"></span>
                <h4>Email</h4>
                <p>bonuongviahe@hutech.com</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- Contact Section End -->

    <!-- Map Begin --> */}
      <div className="map">
        <iframe
          title="Map"
          height="500"
          id="gmap_canvas"
          src="https://maps.google.com/maps?q=B%C3%B2%20N%C6%B0%E1%BB%9Bng%20Ph%E1%BB%91%20-%20B%C3%ACnh%20Th%E1%BA%A1nh&t=&z=15&ie=UTF8&iwloc=&output=embed"
          frameBorder="0"
          scrolling="no"
          marginHeight="0"
          marginWidth="0"
        ></iframe>
      </div>
      {/* <!-- Map End -->

    <!-- Contact Form Begin --> */}
      <div className="contact-form spad">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="contact__form__title">
                <h2>Để lại lời nhắn</h2>
              </div>
            </div>
          </div>
          <AlertMessage info={alert} />
          <form onSubmit={onSubmitFormData}>
            <div className="row">
              <div className="col-lg-12">
                <input
                  type="text"
                  placeholder="Họ Tên"
                  name="fullName"
                  value={fullName}
                  onChange={onChangeFormData}
                  required
                />
              </div>
              <div className="col-lg-6 col-md-6">
                <input
                  type="email"
                  placeholder="Email"
                  name="email"
                  value={email}
                  onChange={onChangeFormData}
                  required
                />
              </div>
              <div className="col-lg-6 col-md-6">
                <input
                  type="text"
                  placeholder="Số điện thoại"
                  name="phone"
                  value={phone}
                  onChange={onChangeFormData}
                  required
                />
              </div>
              <div className="col-lg-12 text-center">
                <textarea
                  placeholder="Chúng tôi trân trọng mọi ý kiến đóng góp của bạn. Đừng ngại liên hệ khi bạn có bất kỳ câu hỏi nào"
                  name="content"
                  value={content}
                  onChange={onChangeFormData}
                  required
                ></textarea>
                <button type="submit" className="site-btn">
                  Gửi lời nhắn
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
      {/* <!-- Contact Form End --> */}
    </>
  );
};

export default Contact;
