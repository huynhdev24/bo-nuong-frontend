import axios from 'axios';

export const apiURL =
  process.env.NODE_ENV !== 'production'
    ? 'http://localhost:8080'
    : 'http://localhost:8080';

export const getCategories = (payload) =>
  axios.get(`${apiURL}/categories`, payload);
export const createCategory = (payload) =>
  axios.post(`${apiURL}/categories`, payload);
export const updateCategory = (payload) =>
  axios.put(`${apiURL}/categories/${payload.id}`, payload);
export const deleteCategory = (payload) =>
  axios.delete(`${apiURL}/categories/${payload}`);

export const getProducts = (payload) =>
  axios.get(`${apiURL}/products`, payload);
export const getLatestProducts = (payload) =>
  axios.get(`${apiURL}/products/latest`, payload);
export const getTopRatingProducts = (payload) =>
  axios.get(`${apiURL}/products/top-rating`, payload);
export const getRelatedProducts = (payload) =>
  axios.get(`${apiURL}/products/related/${payload}`);
export const getDiscountProducts = (payload) =>
  axios.get(`${apiURL}/products/discount`, payload);
export const createProduct = (payload) =>
  axios.post(`${apiURL}/products`, payload);
export const getProductDetail = (payload) =>
  axios.get(`${apiURL}/products/${payload}`);
export const updateProduct = (payload) =>
  axios.put(`${apiURL}/products/${payload.id}`, payload);
export const deleteProduct = (payload) =>
  axios.delete(`${apiURL}/products/${payload}`);

export const getUsers = (payload) => axios.get(`${apiURL}/users/${payload}`);
// export const createUser = (payload) =>
//   axios.post(`${apiURL}/users/${payload.role}`, payload);
export const updateUser = (payload) =>
  axios.put(`${apiURL}/users/${payload.id}`, payload);
// export const deleteUser = (payload) =>
//   axios.delete(`${apiURL}/users/${payload}`);
export const updatePersonal = (payload) =>
  axios.put(`${apiURL}/users/personal/${payload.id}`, payload);

export const getProductComments = (payload) =>
  axios.get(`${apiURL}/comments/${payload}`);
export const createComment = (payload) =>
  axios.post(`${apiURL}/comments`, payload);
export const getAllComments = (payload) =>
  axios.get(`${apiURL}/comments`, payload);
export const updateComment = (payload) =>
  axios.put(`${apiURL}/comments/${payload.id}`, payload);
export const deleteComment = (payload) =>
  axios.delete(`${apiURL}/comments/${payload}`);

export const createOrder = (payload) => axios.post(`${apiURL}/orders`, payload);
export const getOrders = (payload) => axios.get(`${apiURL}/orders`, payload);
export const updateOrder = (payload) =>
  axios.put(`${apiURL}/orders/${payload.id}`, payload);
export const getUserOrders = (payload) =>
  axios.get(`${apiURL}/orders/users/${payload}`, payload);
