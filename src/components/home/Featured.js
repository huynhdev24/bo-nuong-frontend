import { useContext } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { CartContext } from '../../contexts/CartContext';
import { toast$ } from '../../redux/selectors';
import Tooltip from '../layouts/Tooltip';

const Featured = ({ products }) => {
  const { addCart } = useContext(CartContext);
  const toast = useSelector(toast$);

  const onAddCart = (product) => {
    addCart(product);
  };

  return (
    <>
      <Tooltip toast={toast} />
      <section className="featured spad">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="section-title">
                <h2>Sản phẩm của chúng tôi</h2>
              </div>
              {/* <div className="featured__controls">
              <ul>
                <li className="active" data-filter="*">
                  Tất cả
                </li>
                <li data-filter=".a6241b70b2741851255527e42">Nướng</li>
                <li data-filter=".a6241bdca7ef7563ee4e5ed04">Lẩu</li>
                <li data-filter=".a624ae58b61725e061b9eae69">Đồ Tươi</li>
                <li data-filter=".a624ae59461725e061b9eae6a">Khác</li>
              </ul>
            </div> */}
            </div>
          </div>
          <div className="row featured__filter">
            {products?.data?.map((product) => (
              <div
                className={`col-lg-3 col-md-4 col-sm-6 mix`}
                key={product?.id}
              >
                <div className="featured__item">
                  <div
                    className="featured__item__pic set-bg"
                    style={{
                      backgroundImage: `url(${product?.image})`,
                    }}
                  >
                    <ul className="featured__item__pic__hover">
                      <li>
                        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                        <a onClick={() => onAddCart(product)}>
                          <i className="fa fa-shopping-cart"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="featured__item__text">
                    <h6>
                      <Link to={`/products/${product?.id}`}>
                        {product?.name}
                      </Link>
                    </h6>
                    <h5>
                      {String(product?.price).replace(
                        /(.)(?=(\d{3})+$)/g,
                        '$1,'
                      )}{' '}
                      &#8363;
                    </h5>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
    </>
  );
};

export default Featured;
