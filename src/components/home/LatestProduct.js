import React from 'react';
import { Link } from 'react-router-dom';

const LatestProduct = ({ products }) => {
  return (
    <section className="latest-product spad">
      <div className="container">
        <div className="row">
          <div className="col-lg-4 col-md-6">
            <div className="latest-product__text">
              <h4>Mới Nhất</h4>
              <div className="latest-product__slider owl-carousel">
                <div className="latest-product__slider__item">
                  {/* <Link to={`/products/${item.id}`} className="latest-product__item">
                    <div className="latest-product__item__pic">
                      <img src="/img/latest-product/lp-1.jpg" alt="" />
                    </div>
                    <div className="latest-product__item__text">
                      <h6>Crab Pool Security</h6>
                      <span>$30.00</span>
                    </div>
                  </Link>
                  <Link to={`/products/${item.id}`} className="latest-product__item">
                    <div className="latest-product__item__pic">
                      <img src="/img/latest-product/lp-2.jpg" alt="" />
                    </div>
                    <div className="latest-product__item__text">
                      <h6>Crab Pool Security</h6>
                      <span>$30.00</span>
                    </div>
                  </Link>
                  <Link to={`/products/${item.id}`} className="latest-product__item">
                    <div className="latest-product__item__pic">
                      <img src="/img/latest-product/lp-3.jpg" alt="" />
                    </div>
                    <div className="latest-product__item__text">
                      <h6>Crab Pool Security</h6>
                      <span>$30.00</span>
                    </div>
                  </Link> */}
                  {products.latestProducts?.map((item) => (
                    <Link
                      to={`/products/${item.id}`}
                      key={item.id}
                      className="latest-product__item"
                    >
                      <div className="latest-product__item__pic">
                        <img src={item?.image} alt="" />
                      </div>
                      <div className="latest-product__item__text">
                        <h6>{item.name}</h6>
                        <span>
                          {String(item?.price).replace(
                            /(.)(?=(\d{3})+$)/g,
                            '$1,'
                          )}{' '}
                          &#8363;
                        </span>
                      </div>
                    </Link>
                  ))}
                </div>
                <div className="latest-product__slider__item">
                  {products.latestProducts?.map((item) => (
                    <Link
                      to={`/products/${item.id}`}
                      key={item.id}
                      className="latest-product__item"
                    >
                      <div className="latest-product__item__pic">
                        <img src={item?.image} alt="" />
                      </div>
                      <div className="latest-product__item__text">
                        <h6>{item.name}</h6>
                        <span>
                          {String(item?.price).replace(
                            /(.)(?=(\d{3})+$)/g,
                            '$1,'
                          )}{' '}
                          &#8363;
                        </span>
                      </div>
                    </Link>
                  ))}
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="latest-product__text">
              <h4>Top Đánh Giá</h4>
              <div className="latest-product__slider owl-carousel">
                <div className="latest-product__slider__item">
                  {products.topRatingProducts?.map((item) => (
                    <Link
                      to={`/products/${item.id}`}
                      key={item.id}
                      className="latest-product__item"
                    >
                      <div className="latest-product__item__pic">
                        <img src={item?.image} alt="" />
                      </div>
                      <div className="latest-product__item__text">
                        <h6>{item.name}</h6>
                        <span>
                          {String(item?.price).replace(
                            /(.)(?=(\d{3})+$)/g,
                            '$1,'
                          )}{' '}
                          &#8363;
                        </span>
                      </div>
                    </Link>
                  ))}
                </div>
                <div className="latest-product__slider__item">
                  {products.topRatingProducts?.map((item) => (
                    <Link
                      to={`/products/${item.id}`}
                      key={item.id}
                      className="latest-product__item"
                    >
                      <div className="latest-product__item__pic">
                        <img src={item?.image} alt="" />
                      </div>
                      <div className="latest-product__item__text">
                        <h6>{item.name}</h6>
                        <span>
                          {String(item?.price).replace(
                            /(.)(?=(\d{3})+$)/g,
                            '$1,'
                          )}{' '}
                          &#8363;
                        </span>
                      </div>
                    </Link>
                  ))}
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="latest-product__text">
              <h4>Top Giảm Giá</h4>
              <div className="latest-product__slider owl-carousel">
                <div className="latest-product__slider__item">
                  {products.discountProducts?.map((item) => (
                    <Link
                      to={`/products/${item.id}`}
                      key={item.id}
                      className="latest-product__item"
                    >
                      <div className="latest-product__item__pic">
                        <img src={item?.image} alt="" />
                      </div>
                      <div className="latest-product__item__text">
                        <h6>{item.name}</h6>
                        <span>
                          {String(item?.price).replace(
                            /(.)(?=(\d{3})+$)/g,
                            '$1,'
                          )}{' '}
                          &#8363;
                        </span>
                      </div>
                    </Link>
                  ))}
                </div>
                <div className="latest-product__slider__item">
                  {products.discountProducts?.map((item) => (
                    <Link
                      to={`/products/${item.id}`}
                      key={item.id}
                      className="latest-product__item"
                    >
                      <div className="latest-product__item__pic">
                        <img src={item?.image} alt="" />
                      </div>
                      <div className="latest-product__item__text">
                        <h6>{item.name}</h6>
                        <span>
                          {String(item?.price).replace(
                            /(.)(?=(\d{3})+$)/g,
                            '$1,'
                          )}{' '}
                          &#8363;
                        </span>
                      </div>
                    </Link>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default LatestProduct;
