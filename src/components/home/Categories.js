import React from 'react';

const Categories = () => {
  return (
    <section className="categories">
      <div className="container">
        <div className="row">
          <div className="categories__slider owl-carousel">
            <div className="col-lg-3">
              <div
                className="categories__item set-bg"
                style={{
                  backgroundImage: `url(/img/categories/nuong.jpeg)`,
                }}
              >
                <h5>
                  <a href="/products?category=6241b70b2741851255527e42">
                    Món nướng
                  </a>
                </h5>
              </div>
            </div>
            <div className="col-lg-3">
              <div
                className="categories__item set-bg"
                style={{
                  backgroundImage: `url(/img/categories/lau.jpeg)`,
                }}
              >
                <h5>
                  <a href="/products?category=6241bdca7ef7563ee4e5ed04">
                    Món lẩu
                  </a>
                </h5>
              </div>
            </div>
            <div className="col-lg-3">
              <div
                className="categories__item set-bg"
                style={{
                  backgroundImage: `url(/img/categories/thit-tuoi.jpeg)`,
                }}
              >
                <h5>
                  <a href="/products?category=624ae58b61725e061b9eae69">
                    Thịt tươi
                  </a>
                </h5>
              </div>
            </div>
            <div className="col-lg-3">
              <div
                className="categories__item set-bg"
                style={{
                  backgroundImage: `url(/img/categories/nuoc.jpeg)`,
                }}
              >
                <h5>
                  <a href="/products?category=624ae58161725e061b9eae68">
                    Nước uống
                  </a>
                </h5>
              </div>
            </div>
            <div className="col-lg-3">
              <div
                className="categories__item set-bg"
                style={{
                  backgroundImage: `url(/img/categories/khac.jpeg)`,
                }}
              >
                <h5>
                  <a href="/products">Món khác</a>
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Categories;
