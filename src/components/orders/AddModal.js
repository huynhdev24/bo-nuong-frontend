/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  MenuItem,
} from '@mui/material';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  hideModal,
  setCurrentId,
  showModal,
  showToast,
} from '../../redux/actions';
import { updateOrder } from '../../redux/actions/orders';
import { currentId$, orders$, modal$, toast$ } from '../../redux/selectors';
import AlertMessage from '../layouts/AlertMessage';
import Transition from '../layouts/Transition';

const AddModal = () => {
  const dispatch = useDispatch();
  const modal = useSelector(modal$);
  const currentId = useSelector(currentId$);
  const orders = useSelector(orders$);
  const toast = useSelector(toast$);
  const [newOrder, setUpdateOrder] = useState({
    isDelivery: false,
    isPayment: false,
  });
  const { isDelivery, isPayment } = newOrder;
  const currentOrder =
    currentId.id !== 0
      ? orders.data.find((order) => order.id === currentId.id)
      : null;

  useEffect(() => {
    if (currentId.id !== 0) {
      setUpdateOrder({
        isDelivery: currentOrder?.isDelivery,
        isPayment: currentOrder?.isPayment,
      });
      dispatch(showModal());
    } else {
      setUpdateOrder({
        isDelivery: false,
        isPayment: false,
      });
    }
  }, [currentId, dispatch]);

  const onChangeUpdateOrderForm = (event) =>
    setUpdateOrder({ ...newOrder, [event.target.name]: event.target.value });

  const closeDialog = () => {
    setUpdateOrder({
      isDelivery: false,
      isPayment: false,
    });
    dispatch(hideModal());
    if (currentId.id !== 0) dispatch(setCurrentId(0));
  };

  const onSubmit = async (event) => {
    event.preventDefault();
    dispatch(
      updateOrder.updateOrderRequest({
        id: currentId.id,
        ...newOrder,
      })
    );
    dispatch(
      showToast({
        message: 'Vui lòng chờ! Dữ liệu đang được cập nhật...',
        type: 'warning',
      })
    );
  };

  return (
    <Dialog TransitionComponent={Transition} open={modal.show} scroll="body">
      <DialogTitle>CHỈNH SỬA</DialogTitle>
      <DialogContent dividers>
        <AlertMessage info={toast} />
        <TextField
          margin="normal"
          required
          fullWidth
          select
          label="Trang thái giao hàng"
          name="isDelivery"
          onChange={onChangeUpdateOrderForm}
          value={isDelivery}
        >
          <MenuItem value={true}>Đã giao</MenuItem>
          <MenuItem value={false}>Chưa giao</MenuItem>
        </TextField>
        <TextField
          margin="normal"
          required
          fullWidth
          select
          label="Trang thái thanh toán"
          name="isPayment"
          onChange={onChangeUpdateOrderForm}
          value={isPayment}
        >
          <MenuItem value={true}>Đã thanh toán</MenuItem>
          <MenuItem value={false}>Chưa thanh toán</MenuItem>
        </TextField>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={onSubmit}>
          Ok
        </Button>
        <Button onClick={closeDialog}>Cancel</Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddModal;
