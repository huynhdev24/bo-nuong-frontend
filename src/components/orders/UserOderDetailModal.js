/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TableContainer,
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  Paper,
  Typography,
} from '@mui/material';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { orders$ } from '../../redux/selectors';
import Transition from '../layouts/Transition';

const UserOderDetailModal = ({ open, id, handleClose }) => {
  const orders = useSelector(orders$);
  const [newOrder, setUpdateOrder] = useState({
    orderDetail: null,
  });
  const { orderDetail } = newOrder;
  const currentOrder =
    id !== 0 ? orders.userOrders.find((order) => order.id === id) : null;

  useEffect(() => {
    if (id !== 0) {
      setUpdateOrder({
        orderDetail: currentOrder?.orderDetail,
      });
    } else {
      setUpdateOrder({
        orderDetail: null,
      });
    }
  }, [id]);

  return (
    <Dialog TransitionComponent={Transition} open={open} scroll="body">
      <DialogTitle>Chi tiết {id}</DialogTitle>
      <DialogContent dividers>
        <Typography component="legend" sx={{ mb: 3 }}>
          Ghi chú: "{currentOrder?.note}"
        </Typography>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 550 }} size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell>Tên sản phẩm</TableCell>
                <TableCell align="right">Số lượng</TableCell>
                <TableCell align="right">Thành tiền (&#8363;)</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {orderDetail?.map((item) => (
                <TableRow
                  key={item.product.id}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {item.product.name}
                  </TableCell>
                  <TableCell align="right">
                    {String(item.quantity).replace(/(.)(?=(\d{3})+$)/g, '$1,')}
                  </TableCell>
                  <TableCell align="right">
                    {String(item.price).replace(/(.)(?=(\d{3})+$)/g, '$1,')}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Ok</Button>
      </DialogActions>
    </Dialog>
  );
};

export default UserOderDetailModal;
