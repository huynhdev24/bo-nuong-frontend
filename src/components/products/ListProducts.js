/* eslint-disable react-hooks/exhaustive-deps */
import InputUnstyled from '@mui/base/InputUnstyled';
import ClearIcon from '@mui/icons-material/Clear';
import { Box, IconButton, InputAdornment, Pagination } from '@mui/material';
import queryString from 'query-string';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useLocation } from 'react-router-dom';

const ListProducts = ({ products, onAddCart, minValue, maxValue }) => {
  const location = useLocation();
  const { category } = queryString.parse(location.search);
  const itemsPerPage = 3;
  const [searchString, setSearchString] = useState('');
  const [page, setPage] = useState(1);

  const filterProduct = () => {
    return products.data.filter(
      (item) =>
        (category ? item.category.id === category : true) &&
        item.price >= minValue &&
        item.price <= maxValue &&
        item.name
          .toLowerCase()
          .normalize('NFD')
          .replace(/[\u0300-\u036f]/g, '')
          .includes(
            searchString
              .toLowerCase()
              .normalize('NFD')
              .replace(/[\u0300-\u036f]/g, '')
          )
    );
  };

  const [noOfPages, setNoOfPages] = useState(
    Math.ceil(filterProduct().length / itemsPerPage)
  );

  useEffect(() => {
    setNoOfPages(Math.ceil(filterProduct().length / itemsPerPage));
  }, [maxValue, minValue, products.data, searchString, category]);

  const handlePageChange = (_, value) => {
    setPage(value);
  };

  const onChange = (event) => {
    setSearchString(event.target.value);
  };

  return (
    <>
      <div className="filter__item">
        <div className="row">
          <div className="col-lg-6 col-md-6">
            <div className="filter__found">
              <h6>
                <span>{filterProduct().length}</span> Sản phẩm được tìm thấy
              </h6>
            </div>
          </div>
          <div className="col-lg-6 col-md-6">
            <div className="hero__search__form">
              <form
                onSubmit={(event) => {
                  event.preventDefault();
                }}
              >
                <InputUnstyled
                  type="text"
                  value={searchString}
                  onChange={onChange}
                  placeholder="Nhập tên sản phẩm cần tìm"
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="clear search string"
                        onClick={() => setSearchString('')}
                        edge="end"
                        style={{ right: '15px' }}
                      >
                        <ClearIcon />
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        {filterProduct()
          ?.slice((page - 1) * itemsPerPage, page * itemsPerPage)
          .map((product) => (
            <div className="col-lg-4 col-md-6 col-sm-6" key={product?.id}>
              <div className="product__item">
                <div
                  className="product__item__pic set-bg"
                  style={{
                    backgroundImage: `url(${product?.image})`,
                  }}
                >
                  <ul className="product__item__pic__hover">
                    <li>
                      {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                      <a onClick={() => onAddCart(product)}>
                        <i className="fa fa-shopping-cart"></i>
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="product__item__text">
                  <h6>
                    <Link to={`/products/${product?.id}`}>{product?.name}</Link>
                  </h6>
                  <h5>
                    {String(product?.price).replace(/(.)(?=(\d{3})+$)/g, '$1,')}{' '}
                    &#8363;
                  </h5>
                </div>
              </div>
            </div>
          ))}
      </div>
      <Box component="div" style={{ padding: '10px', margin: '10px' }}>
        <Pagination
          count={noOfPages}
          page={page}
          onChange={handlePageChange}
          defaultPage={1}
          color="primary"
          variant="outlined"
          showFirstButton
          showLastButton
          sx={{
            left: '50%',
            transform: 'translateX(-50%)',
            position: 'absolute',
          }}
        />
      </Box>
    </>
  );
};

export default ListProducts;
