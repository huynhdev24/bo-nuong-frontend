/* eslint-disable react-hooks/exhaustive-deps */
import {
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  MenuItem,
  TextField,
} from '@mui/material';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  hideModal,
  setCurrentId,
  showModal,
  showToast,
} from '../../redux/actions';
import { getCategories } from '../../redux/actions/categories';
import { createProduct, updateProduct } from '../../redux/actions/products';
import {
  currentId$,
  categories$,
  modal$,
  products$,
  toast$,
} from '../../redux/selectors';
import AlertMessage from '../layouts/AlertMessage';
import Transition from '../layouts/Transition';

const AddModal = () => {
  const dispatch = useDispatch();
  const [alert, setAlert] = useState(null);
  const modal = useSelector(modal$);
  const currentId = useSelector(currentId$);
  const toast = useSelector(toast$);
  const products = useSelector(products$);
  const categories = useSelector(categories$);
  const [newProduct, setNewProduct] = useState({
    name: '',
    description: '',
    price: 0,
    quantity: 0,
    unit: '',
    discount: 0,
    image: '',
    categoryId: '',
  });
  const {
    name,
    description,
    image,
    price,
    quantity,
    unit,
    discount,
    categoryId,
  } = newProduct;
  const currentProduct =
    currentId.id !== 0
      ? products.data.find((admin) => admin.id === currentId.id)
      : null;

  useEffect(() => {
    if (currentId.id !== 0) {
      setNewProduct({
        name: currentProduct?.name,
        description: currentProduct?.description,
        price: currentProduct?.price,
        quantity: currentProduct?.quantity,
        unit: currentProduct?.unit,
        discount: currentProduct?.discount,
        image: currentProduct?.image,
        categoryId: currentProduct?.category.id,
      });
      dispatch(showModal());
    } else {
      setNewProduct({
        name: '',
        description: '',
        price: 0,
        quantity: 0,
        unit: '',
        discount: 0,
        image: '',
        categoryId: '',
      });
    }
  }, [currentId, dispatch]);

  useEffect(() => {
    dispatch(getCategories.getCategoriesRequest());
  }, [dispatch]);

  const onChangeNewProductForm = (event) => {
    setNewProduct({ ...newProduct, [event.target.name]: event.target.value });
  };

  const toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  const handleFileChange = async (event) => {
    const base64image = await toBase64(event.target.files[0]);
    setNewProduct({ ...newProduct, image: base64image });
  };

  const closeDialog = () => {
    setNewProduct({
      name: '',
      description: '',
      price: 0,
      quantity: 0,
      unit: '',
      discount: 0,
      image: '',
      categoryId: '',
    });
    dispatch(hideModal());
    if (currentId.id !== 0) dispatch(setCurrentId(0));
  };

  const onSubmit = async (event) => {
    event.preventDefault();
    if (!name || !description || !image) {
      setAlert({
        type: 'warning',
        message: 'Vui lòng điền đầy đủ thông tin',
      });
      setTimeout(() => setAlert(null), 5000);
      return;
    }
    setAlert(null);
    if (currentId.id === 0) {
      dispatch(createProduct.createProductRequest(newProduct));
      dispatch(
        showToast({
          message: 'Vui lòng chờ! Dữ liệu đang được cập nhật...',
          type: 'warning',
        })
      );
    } else {
      dispatch(
        updateProduct.updateProductRequest({
          id: currentId.id,
          ...newProduct,
        })
      );
      dispatch(
        showToast({
          message: 'Vui lòng chờ! Dữ liệu đang được cập nhật...',
          type: 'warning',
        })
      );
    }
    dispatch(hideModal());
    if (currentId.id !== 0) dispatch(setCurrentId(0));
  };

  return (
    <Dialog TransitionComponent={Transition} open={modal.show} scroll="body">
      <DialogTitle>{currentId.id === 0 ? 'THÊM' : 'CHỈNH SỬA'}</DialogTitle>
      <DialogContent dividers>
        <Box component="form" onSubmit={onSubmit}>
          <div style={{ marginBottom: '20px' }}>
            {alert && <AlertMessage info={alert} />}
            {!alert && <AlertMessage info={toast} />}
          </div>
          <TextField
            margin="dense"
            type="text"
            required
            fullWidth
            variant="standard"
            autoFocus
            label="Tên sản phẩm"
            name="name"
            value={name}
            onChange={onChangeNewProductForm}
          />
          <TextField
            margin="dense"
            multiline
            required
            fullWidth
            variant="standard"
            label="Mô tả"
            name="description"
            value={description}
            onChange={onChangeNewProductForm}
          />
          <TextField
            margin="dense"
            type="number"
            required
            fullWidth
            variant="standard"
            autoFocus
            label="Giá"
            name="price"
            value={price}
            onChange={onChangeNewProductForm}
          />
          <TextField
            margin="dense"
            type="number"
            required
            fullWidth
            variant="standard"
            autoFocus
            label="Số lượng"
            name="quantity"
            value={quantity}
            onChange={onChangeNewProductForm}
          />
          <TextField
            margin="dense"
            type="text"
            required
            fullWidth
            variant="standard"
            autoFocus
            label="Đơn vị"
            name="unit"
            value={unit}
            onChange={onChangeNewProductForm}
          />
          <TextField
            margin="dense"
            type="number"
            required
            fullWidth
            variant="standard"
            autoFocus
            label="Gỉam giá"
            name="discount"
            value={discount}
            onChange={onChangeNewProductForm}
          />
          <TextField
            margin="dense"
            required
            fullWidth
            variant="standard"
            select
            label="Loại sản phẩm"
            name="categoryId"
            value={categoryId}
            onChange={onChangeNewProductForm}
            sx={{ marginBottom: '20px' }}
          >
            {categories.data.map((category) => (
              <MenuItem key={category.id} value={category.id}>
                {category.name}
              </MenuItem>
            ))}
          </TextField>
          <TextField
            margin="dense"
            type="file"
            accept="image/*"
            multiple={false}
            fullWidth
            variant="standard"
            label="Ảnh"
            helperText="Chọn ảnh"
            name="image"
            onChange={handleFileChange}
          />
          <Button
            fullWidth
            type="submit"
            variant="contained"
            sx={{ mt: 3, mb: 1 }}
          >
            Ok
          </Button>
          <Button fullWidth onClick={closeDialog}>
            Cancel
          </Button>
        </Box>
      </DialogContent>
    </Dialog>
  );
};

export default AddModal;
