import { Link } from 'react-router-dom';

const LatestProducts = ({ products }) => {
  return (
    <div className="sidebar__item">
      <div className="latest-product__text">
        <h4>Mới nhất</h4>
        <div className="latest-product__slider owl-carousel">
          <div className="latest-prdouct__slider__item">
            {products.latestProducts?.map((item) => (
              <Link
                to={`/products/${item.id}`}
                key={item.id}
                className="latest-product__item"
              >
                <div className="latest-product__item__pic">
                  <img src={item?.image} alt="" />
                </div>
                <div className="latest-product__item__text">
                  <h6>{item.name}</h6>
                  <span>
                    {String(item?.price).replace(/(.)(?=(\d{3})+$)/g, '$1,')}{' '}
                    &#8363;
                  </span>
                </div>
              </Link>
            ))}
          </div>
          <div className="latest-prdouct__slider__item">
            {products.latestProducts?.map((item) => (
              <Link
                to={`/products/${item.id}`}
                key={item.id}
                className="latest-product__item"
              >
                <div className="latest-product__item__pic">
                  <img src={item?.image} alt="" />
                </div>
                <div className="latest-product__item__text">
                  <h6>{item.name}</h6>
                  <span>
                    {String(item?.price).replace(/(.)(?=(\d{3})+$)/g, '$1,')}{' '}
                    &#8363;
                  </span>
                </div>
              </Link>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default LatestProducts;
