import { useLocation } from 'react-router-dom';
import { Link } from 'react-router-dom';
import queryString from 'query-string';

const ListCategories = ({ categories }) => {
  const location = useLocation();
  const { category } = queryString.parse(location.search);

  return (
    <div className="sidebar__item">
      <h4>Loại sản phẩm</h4>
      <ul>
        <li>
          <Link
            to="/products"
            style={{ color: !category ? '#7fad39' : '#1c1c1c' }}
          >
            Tất cả
          </Link>
        </li>
        {categories.data.map((item) => (
          <li key={item.id}>
            <Link
              to={`/products?category=${item.id}`}
              style={{ color: category === item.id ? '#7fad39' : '#1c1c1c' }}
            >
              {item.name}
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ListCategories;
