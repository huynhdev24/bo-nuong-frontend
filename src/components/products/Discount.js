import { Link } from 'react-router-dom';

const Discount = ({ products, onAddCart }) => {
  return (
    <div className="product__discount">
      <div className="section-title product__discount__title">
        <h2>Đang giảm giá </h2>
      </div>
      <div className="row">
        {products.discountProducts.map((product) => (
          <div className="col-lg-4" key={product.id}>
            <div className="product__discount__item">
              <div
                className="product__discount__item__pic set-bg"
                style={{
                  backgroundImage: `url(${product?.image})`,
                }}
              >
                <div className="product__discount__percent">
                  -
                  {String(product?.discount).replace(
                    /(.)(?=(\d{3})+$)/g,
                    '$1,'
                  )}
                  &#8363;
                </div>
                <ul className="product__item__pic__hover">
                  <li>
                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                    <a onClick={() => onAddCart(product)}>
                      <i className="fa fa-shopping-cart"></i>
                    </a>
                  </li>
                </ul>
              </div>
              <div className="product__discount__item__text">
                <span>{product.category.name}</span>
                <h5>
                  <Link to={`/products/${product?.id}`}>{product.name}</Link>
                </h5>
                <div className="product__item__price">
                  {String(product?.price - product?.discount).replace(
                    /(.)(?=(\d{3})+$)/g,
                    '$1,'
                  )}{' '}
                  &#8363;{' '}
                  <span>
                    {String(product?.price).replace(/(.)(?=(\d{3})+$)/g, '$1,')}{' '}
                    &#8363;
                  </span>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Discount;
