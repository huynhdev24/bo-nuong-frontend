import Slider from '@mui/material/Slider';
import { useState } from 'react';

const PriceRangeSlider = ({ minValue, setMinValue, maxValue, setMaxValue }) => {
  const minDistance = 500000;
  const [value, setValue] = useState([0, 2500000]);

  const handleChange = (_, newValue, activeThumb) => {
    if (!Array.isArray(newValue)) {
      return;
    }

    if (newValue[1] - newValue[0] < minDistance) {
      if (activeThumb === 0) {
        const clamped = Math.min(newValue[0], 2500000 - minDistance);
        setValue([clamped, clamped + minDistance]);
        setMinValue(clamped);
        setMaxValue(clamped + minDistance);
      } else {
        const clamped = Math.max(newValue[1], minDistance);
        setValue([clamped - minDistance, clamped]);
        setMinValue(clamped - minDistance);
        setMaxValue(clamped);
      }
    } else {
      setValue(newValue);
      setMinValue(newValue[0]);
      setMaxValue(newValue[1]);
    }
  };

  const valueLabelFormat = (value) => {
    return `${String(value).replace(/(.)(?=(\d{3})+$)/g, '$1,')} ₫`;
  };

  return (
    <div className="sidebar__item">
      <h4>Giá Tiền</h4>
      <div className="price-range-wrap">
        <Slider
          getAriaLabel={() => 'price range slider'}
          value={value}
          onChange={handleChange}
          valueLabelDisplay="auto"
          disableSwap
          min={0}
          max={2500000}
          step={5000}
          color="error"
          valueLabelFormat={valueLabelFormat}
        />
        <div className="range-slider">
          <div className="price-input">
            <input
              type="text"
              value={`${String(minValue).replace(
                /(.)(?=(\d{3})+$)/g,
                '$1,'
              )} ₫`}
              readOnly
            />
            <input
              type="text"
              value={`${String(maxValue).replace(
                /(.)(?=(\d{3})+$)/g,
                '$1,'
              )} ₫`}
              readOnly
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default PriceRangeSlider;
