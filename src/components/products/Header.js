const Header = () => {
  return (
    <section
      className="breadcrumb-section set-bg"
      style={{
        backgroundImage: `url(img/breadcrumb.jpg)`,
      }}
    >
      <div className="container">
        <div className="row">
          <div className="col-lg-12 text-center">
            <div className="breadcrumb__text">
              <h2>Danh mục sản phẩm</h2>
              <div className="breadcrumb__option">
                <a href="/">Trang chủ</a>
                <span>Danh mục sản phẩm</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Header;
