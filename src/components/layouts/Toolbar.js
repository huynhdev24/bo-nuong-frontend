import AddIcon from '@mui/icons-material/Add';
import { Button } from '@mui/material';
import {
  GridToolbarContainer,
  GridToolbarExport,
  GridToolbarFilterButton,
} from '@mui/x-data-grid';
import { useLocation } from 'react-router-dom';

export default function EditToolbar({ setShowModal }) {
  const location = useLocation();

  return (
    <>
      <GridToolbarContainer>
        <GridToolbarFilterButton />
        <GridToolbarExport />
        {location.pathname.split('/')[2] !== 'customers' &&
          location.pathname.split('/')[2] !== 'comments' &&
          location.pathname.split('/')[2] !== 'orders' &&
          location.pathname.split('/')[2] !== 'revenue' &&
          location.pathname.split('/')[2] !== 'admins' &&
          location.pathname.split('/')[1] === 'admin' && (
            <Button
              color="primary"
              startIcon={<AddIcon />}
              onClick={setShowModal}
              sx={{ fontSize: '0.85rem' }}
            >
              Thêm
            </Button>
          )}
      </GridToolbarContainer>
    </>
  );
}
