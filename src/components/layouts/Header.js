import { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useHistory, useLocation } from 'react-router-dom';
import { AuthContext } from '../../contexts/AuthContext';

const Header = () => {
  const {
    authState: { user, isAuthenticated },
    logoutUser,
  } = useContext(AuthContext);
  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    Array.from(document.querySelectorAll('.header__menu ul li a')).forEach(
      function (item) {
        item.parentElement.classList.remove('active');
        if (item.href.split('/')[3] === location.pathname.split('/')[1])
          item.parentElement.classList.add('active');
      }
    );
  }, [location.pathname]);

  let authButton;
  if (isAuthenticated) {
    authButton = (
      <>
        <Link to="/login">
          <i className="fa fa-user"></i> Xin chào {user?.fullName}
        </Link>
        <span className="arrow_carrot-down"></span>
        <ul>
          <li>
            {user?.role === 'ADMIN' && (
              <p
                onClick={() => history.push('/admin/products')}
                style={{
                  fontSize: '14px',
                  color: '#ffffff',
                  padding: '0px 10px',
                  marginTop: 'unset',
                  marginBottom: 'unset',
                }}
              >
                Quản lí trang web
              </p>
            )}
          </li>
          <li>
            <p
              onClick={() => history.push('/orders-history')}
              style={{
                fontSize: '14px',
                color: '#ffffff',
                padding: '0px 10px',
                marginTop: 'unset',
                marginBottom: 'unset',
              }}
            >
              Lịch sử mua hàng
            </p>
          </li>
          <li>
            <p
              onClick={() => history.push('/personal-info')}
              style={{
                fontSize: '14px',
                color: '#ffffff',
                padding: '0px 10px',
                marginTop: 'unset',
                marginBottom: 'unset',
              }}
            >
              Thông tin cá nhân
            </p>
          </li>
          <li>
            <p
              onClick={logoutUser}
              style={{
                fontSize: '14px',
                color: '#ffffff',
                padding: '0px 10px',
                marginTop: 'unset',
                marginBottom: 'unset',
              }}
            >
              Đăng xuất
            </p>
          </li>
        </ul>
      </>
    );
  } else {
    authButton = (
      <Link to="/login">
        <i className="fa fa-user"></i> Đăng nhập
      </Link>
    );
  }

  return (
    <>
      {/* <!-- Page Preloder --> */}
      <div id="preloder">
        <div className="loader"></div>
      </div>

      {/* <!-- Humberger Begin --> */}
      <div className="humberger__menu__overlay"></div>
      <div className="humberger__menu__wrapper">
        <div className="humberger__menu__logo">
          <a href="/">
            <img src="/img/logo.png" alt="" />
          </a>
        </div>
        {/* <div className="humberger__menu__cart">
          <ul>
            <li>
              <a href="/">
                <i className="fa fa-heart"></i> <span>1</span>
              </a>
            </li>
            <li>
              <a href="/">
                <i className="fa fa-shopping-bag"></i> <span>3</span>
              </a>
            </li>
          </ul>
          <div className="header__cart__price">
            item: <span>$150.00</span>
          </div>
        </div> */}
        <div className="humberger__menu__widget">
          <div className="header__top__right__language">
            <img src="/img/language.png" alt="" width="20" />
            <div>Tiếng Việt</div>
            <span className="arrow_carrot-down"></span>
            <ul>
              <li>
                <a href="/">Tiếng Việt</a>
              </li>
            </ul>
          </div>
          <div className="header__top__right__language">{authButton}</div>
        </div>
        <nav className="humberger__menu__nav mobile-menu">
          <ul>
            <li className="active">
              <a href="/">Trang chủ</a>
            </li>
            <li>
              <a href="/products">Sản phẩm</a>
            </li>
            {/* <li>
              <a href="/">Pages</a>
              <ul className="header__menu__dropdown">
                <li>
                  <Link to="./shop-details.html">Shop Details</Link>
                </li>
                <li>
                  <Link to="./shoping-cart.html">Shoping Cart</Link>
                </li>
                <li>
                  <Link to="./checkout.html">Check Out</Link>
                </li>
                <li>
                  <Link to="./blog-details.html">Blog Details</Link>
                </li>
              </ul>
            </li> */}
            <li>
              <Link to="/cart">Giỏ hàng</Link>
            </li>
            <li>
              <Link to="/contact">Liên hệ</Link>
            </li>
          </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <div className="header__top__right__social">
          <a href="https://www.facebook.com/B%C3%B2-N%C6%B0%E1%BB%9Bng-V%E1%BB%89a-H%C3%A8-107187518619137">
            <i className="fa fa-facebook"></i>
          </a>
          <a href="https://github.com/nhuthuynhphu">
            <i className="fa fa-github"></i>
          </a>
          <a href="https://linkedin.com/in/nhuthuynhphu210">
            <i className="fa fa-linkedin"></i>
          </a>
          <a href="https://instagram.com/huynh.js">
            <i className="fa fa-instagram"></i>
          </a>
        </div>
        <div className="humberger__menu__contact">
          <ul>
            <li>
              <i className="fa fa-envelope"></i> bonuongviahe@hutech.com
            </li>
            <li>Free ship cho đơn hàng trên 999K</li>
          </ul>
        </div>
      </div>
      {/* <!-- Humberger End -->

    <!-- Header Section Begin --> */}
      <header className="header">
        <div className="header__top">
          <div className="container">
            <div className="row">
              <div className="col-lg-6 col-md-6">
                <div className="header__top__left">
                  <ul>
                    <li>
                      <i className="fa fa-envelope"></i> bonuongviahe@hutech.com
                    </li>
                    <li>Free ship cho đơn hàng trên 999K</li>
                  </ul>
                </div>
              </div>
              <div className="col-lg-6 col-md-6">
                <div className="header__top__right">
                  <div className="header__top__right__social">
                    <a href="https://www.facebook.com/B%C3%B2-N%C6%B0%E1%BB%9Bng-V%E1%BB%89a-H%C3%A8-107187518619137">
                      <i className="fa fa-facebook"></i>
                    </a>
                    <a href="https://github.com/nhuthuynhphu">
                      <i className="fa fa-github"></i>
                    </a>
                    <a href="https://linkedin.com/in/nhuthuynhphu210">
                      <i className="fa fa-linkedin"></i>
                    </a>
                    <a href="https://instagram.com/huynh.js">
                      <i className="fa fa-instagram"></i>
                    </a>
                  </div>
                  <div className="header__top__right__language">
                    <img src="/img/language.png" alt="" width="20" />
                    <div>Tiếng Việt</div>
                    <span className="arrow_carrot-down"></span>
                    <ul>
                      <li>
                        <a href="/">Tiếng Việt</a>
                      </li>
                    </ul>
                  </div>
                  <div className="header__top__right__language">
                    {authButton}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-lg-3">
              <div className="header__logo">
                <a href="/">
                  <img src="/img/logo.png" alt="" />
                </a>
              </div>
            </div>
            <div className="col-lg-6">
              <nav className="header__menu">
                <ul>
                  <li>
                    <a href="/">Trang chủ</a>
                  </li>
                  <li>
                    <a href="/products">Sản phẩm</a>
                  </li>
                  <li>
                    <Link to="/cart">Giỏ hàng</Link>
                  </li>
                  <li>
                    <Link to="/contact">Liên hệ</Link>
                  </li>
                </ul>
              </nav>
            </div>
            <div className="col-lg-3" style={{ marginTop: '12px' }}>
              <div className="hero__search__phone">
                <div className="hero__search__phone__icon">
                  <i className="fa fa-phone"></i>
                </div>
                <div className="hero__search__phone__text">
                  <h5>0947.828.163</h5>
                  <span>Hỗ trợ 24/7</span>
                </div>
              </div>
            </div>
          </div>
          <div className="humberger__open">
            <i className="fa fa-bars"></i>
          </div>
        </div>
      </header>
      {/* <!-- Header Section End -->*/}
    </>
  );
};

export default Header;
