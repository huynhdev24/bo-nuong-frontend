import { DataGrid } from '@mui/x-data-grid';
import Tooltip from './Tooltip';
import CustomNoRowsOverlay from './NoRows';
import EditToolbar from './Toolbar';

const DataTable = ({
  component: AddModal,
  toast,
  data,
  columns,
  rowsPerPage,
  handleChangeRowsPerPage,
  setShowModal,
}) => {
  return (
    <>
      <AddModal />
      <Tooltip toast={toast} />
      <div
        style={{
          height: '700px',
          width: '95%',
          margin: '10px auto',
          '& .actions': {
            color: 'text.secondary',
          },
          '& .textPrimary': {
            color: 'text.primary',
          },
        }}
      >
        <DataGrid
          rows={data}
          columns={columns}
          pageSize={rowsPerPage}
          onPageSizeChange={(newPageSize) =>
            handleChangeRowsPerPage(newPageSize)
          }
          rowsPerPageOptions={[10, 20, 40]}
          pagination
          editMode="row"
          components={{
            Toolbar: EditToolbar,
            NoRowsOverlay: CustomNoRowsOverlay,
          }}
          componentsProps={{
            toolbar: { setShowModal },
          }}
          sx={{
            backgroundColor: 'white',
            boxShadow: 3,
          }}
        />
      </div>
    </>
  );
};

export default DataTable;
