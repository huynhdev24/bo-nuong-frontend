import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import CategoryIcon from '@mui/icons-material/Category';
import FastfoodIcon from '@mui/icons-material/Fastfood';
import ForumIcon from '@mui/icons-material/Forum';
import GroupIcon from '@mui/icons-material/Group';
import ReceiptIcon from '@mui/icons-material/Receipt';
import TimelineIcon from '@mui/icons-material/Timeline';
import {
  Box,
  Divider,
  Drawer,
  ListItem,
  ListItemIcon,
  ListItemText,
  useMediaQuery,
} from '@mui/material';
import PropTypes from 'prop-types';
import { Fragment } from 'react';
import { Link, useLocation } from 'react-router-dom';

export const Sidebar = (props) => {
  const { open, onClose } = props;
  const location = useLocation();
  const lgUp = useMediaQuery((theme) => theme.breakpoints.up('lg'), {
    defaultMatches: true,
    noSsr: false,
  });

  const items = [
    {
      href: '/admin/products',
      icon: <FastfoodIcon fontSize="small" />,
      title: 'Sản phẩm',
    },
    {
      href: '/admin/categories',
      icon: <CategoryIcon fontSize="small" />,
      title: 'Loại sản phẩm',
    },
    {
      href: '/admin/comments',
      icon: <ForumIcon fontSize="small" />,
      title: 'Bình luận',
    },
    {
      href: '/admin/customers',
      icon: <GroupIcon fontSize="small" />,
      title: 'Khách hàng',
    },
    {
      href: '/admin/orders',
      icon: <ReceiptIcon fontSize="small" />,
      title: 'Đơn hàng',
    },
    {
      href: '/admin/revenue',
      icon: <TimelineIcon fontSize="small" />,
      title: 'Doanh thu',
    },
    {
      href: '/admin/admins',
      icon: <AdminPanelSettingsIcon fontSize="small" />,
      title: 'Admins',
    },
  ];

  const content = (
    <>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          height: '100%',
        }}
      >
        <Box sx={{ p: 3 }}>Quản lý website</Box>
        <Divider
          sx={{
            borderColor: '#2D3748',
            mb: 3,
          }}
        />
        <Box sx={{ flexGrow: 1 }}>
          {items.map((item, index) => (
            <Fragment key={index}>
              <Link to={item.href}>
                <ListItem
                  sx={{
                    backgroundColor:
                      location.pathname.split('/')[2] ===
                        item.href.split('/')[2] && 'rgba(255,255,255, 0.08)',
                    borderRadius: 1,
                    color:
                      location.pathname.split('/')[2] ===
                      item.href.split('/')[2]
                        ? 'secondary.main'
                        : 'neutral.300',
                    fontWeight:
                      location.pathname.split('/')[2] ===
                        item.href.split('/')[2] && 'fontWeightBold',
                    justifyContent: 'flex-start',
                    textAlign: 'left',
                    textTransform: 'none',
                    width: '100%',
                    '& .MuiButton-startIcon': {
                      color:
                        location.pathname.split('/')[2] ===
                        item.href.split('/')[2]
                          ? 'secondary.main'
                          : 'neutral.400',
                    },
                    '&:hover': {
                      backgroundColor: 'rgba(255,255,255, 0.08)',
                    },
                  }}
                >
                  <ListItemIcon>{item.icon}</ListItemIcon>
                  <ListItemText sx={{ flexGrow: 1 }}>{item.title}</ListItemText>
                </ListItem>
              </Link>
            </Fragment>
          ))}
        </Box>
      </Box>
    </>
  );

  if (lgUp) {
    return (
      <Drawer
        anchor="left"
        open
        PaperProps={{
          sx: {
            backgroundColor: 'neutral.900',
            color: '#FFFFFF',
            width: '250px',
          },
        }}
        variant="permanent"
      >
        {content}
      </Drawer>
    );
  }

  return (
    <Drawer
      anchor="left"
      onClose={onClose}
      open={open}
      PaperProps={{
        sx: {
          backgroundColor: 'neutral.900',
          color: '#FFFFFF',
          width: '250px',
        },
      }}
      sx={{ zIndex: (theme) => theme.zIndex.appBar + 100 }}
      variant="temporary"
    >
      {content}
    </Drawer>
  );
};

Sidebar.propTypes = {
  onClose: PropTypes.func,
  open: PropTypes.bool,
};
