import { Route } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';

const Layout = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => (
        <>
          <Header />
          <Component {...rest} {...props} />
          <Footer />
        </>
      )}
    />
  );
};

export default Layout;
