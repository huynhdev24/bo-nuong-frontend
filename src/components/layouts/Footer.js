import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => {
  return (
    <>
      {/*<!-- Footer Section Begin --> */}
      <footer className="footer spad">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="footer__about">
                <div className="footer__about__logo">
                  <a href="/">
                    <img src="/img/logo.png" alt="" />
                  </a>
                </div>
                <ul>
                  <li>Địa chỉ: Số 11, đường 85, P.Hiệp Phú, Quận 9, TP.HCM</li>
                  <li>Điện thoại: 0365.204.529</li>
                  <li>Email: bonuongviahe@hutech.com</li>
                </ul>
              </div>
            </div>
            <div className="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
              <div className="footer__widget">
                <h6>Danh mục</h6>
                <ul>
                  <li>
                    <a href="/">Trang chủ</a>
                  </li>
                  <li>
                    <a href="/products">Sản phẩm</a>
                  </li>
                  <li>
                    <Link to="/cart">Giỏ hàng</Link>
                  </li>
                  <li>
                    <Link to="/contact">Liên hệ</Link>
                  </li>
                  <li>
                    <Link to="/register">Đăng ký</Link>
                  </li>
                  <li>
                    <Link to="/login">Đăng nhập</Link>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-4 col-md-12">
              <div className="footer__widget">
                <h6>Liên kết với chúng tôi</h6>
                <p>Nhận thông tin ưu đãi nhanh nhất</p>
                <div className="footer__widget__social">
                  <a href="https://www.facebook.com/huynh2357">
                    <i className="fa fa-facebook"></i>
                  </a>
                  <a href="https://github.com/huynhdev24">
                    <i className="fa fa-github"></i>
                  </a>
                  <a href="https://linkedin.com/">
                    <i className="fa fa-linkedin"></i>
                  </a>
                  <a href="https://instagram.com/">
                    <i className="fa fa-instagram"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="footer__copyright">
                <div className="footer__copyright__text">
                  <p>
                    Bản quyền &copy; {new Date().getFullYear()} Trang web được
                    phát triển bằng nhiệt huyết{' '}
                    <i className="fa fa-heart" aria-hidden="true"></i> của{' '}
                    <a
                      href="https://www.facebook.com/B%C3%B2-N%C6%B0%E1%BB%9Bng-V%E1%BB%89a-H%C3%A8-107187518619137"
                      target="_blank"
                      rel="noreferrer"
                    >
                      19DTHD4
                    </a>
                  </p>
                </div>
                <div className="footer__copyright__payment">
                  <img src="/img/payment-item.png" alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
      {/* <!-- Footer Section End --> */}
    </>
  );
};

export default Footer;
