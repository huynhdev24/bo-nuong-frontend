/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from '@mui/material';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  hideModal,
  setCurrentId,
  showModal,
  showToast,
} from '../../redux/actions';
import { createCategory, updateCategory } from '../../redux/actions/categories';
import { currentId$, categories$, modal$, toast$ } from '../../redux/selectors';
import AlertMessage from '../layouts/AlertMessage';
import Transition from '../layouts/Transition';

const AddModal = () => {
  const dispatch = useDispatch();
  const [alert, setAlert] = useState(null);
  const modal = useSelector(modal$);
  const currentId = useSelector(currentId$);
  const categories = useSelector(categories$);
  const toast = useSelector(toast$);
  const [newCategory, setNewCategory] = useState({
    name: '',
  });
  const { name } = newCategory;
  const currentCategory =
    currentId.id !== 0
      ? categories.data.find((lecture) => lecture.id === currentId.id)
      : null;

  useEffect(() => {
    if (currentId.id !== 0) {
      setNewCategory({
        name: currentCategory?.name,
      });
      dispatch(showModal());
    } else {
      setNewCategory({
        name: '',
      });
    }
  }, [currentId, dispatch]);

  const onChangeNewCategoryForm = (event) =>
    setNewCategory({ ...newCategory, [event.target.name]: event.target.value });

  const closeDialog = () => {
    setNewCategory({
      name: '',
    });
    dispatch(hideModal());
    if (currentId.id !== 0) dispatch(setCurrentId(0));
  };

  const onSubmit = async (event) => {
    event.preventDefault();
    if (!name) {
      setAlert({
        type: 'warning',
        message: 'Tên loại sản phẩm bị bỏ trống.',
      });
      setTimeout(() => setAlert(null), 5000);
      return;
    }
    setAlert(null);
    if (currentId.id === 0) {
      dispatch(createCategory.createCategoryRequest(newCategory));
      dispatch(
        showToast({
          message: 'Vui lòng chờ! Dữ liệu đang được cập nhật...',
          type: 'warning',
        })
      );
    } else {
      dispatch(
        updateCategory.updateCategoryRequest({
          id: currentId.id,
          ...newCategory,
        })
      );
      dispatch(
        showToast({
          message: 'Vui lòng chờ! Dữ liệu đang được cập nhật...',
          type: 'warning',
        })
      );
    }
  };

  return (
    <Dialog TransitionComponent={Transition} open={modal.show} scroll="body">
      <DialogTitle>{currentId.id === 0 ? 'THÊM' : 'CHỈNH SỬA'}</DialogTitle>
      <DialogContent dividers>
        {alert && <AlertMessage info={alert} />}
        {!alert && <AlertMessage info={toast} />}
        <TextField
          margin="dense"
          label="Tên loại sản phẩm"
          type="text"
          name="name"
          required
          fullWidth
          variant="standard"
          value={name}
          onChange={onChangeNewCategoryForm}
        />
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={onSubmit}>
          Ok
        </Button>
        <Button onClick={closeDialog}>Cancel</Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddModal;
