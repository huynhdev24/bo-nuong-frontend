/* eslint-disable no-useless-escape */
/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  MenuItem,
  TextField,
} from '@mui/material';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  hideModal,
  setCurrentId,
  showModal,
  showToast,
} from '../../redux/actions';
import { updateAdmin } from '../../redux/actions/admins';
import { currentId$, modal$, admins$ } from '../../redux/selectors';
import Transition from '../layouts/Transition';

const AddModal = () => {
  const dispatch = useDispatch();
  const modal = useSelector(modal$);
  const admins = useSelector(admins$);
  const currentId = useSelector(currentId$);
  const [newAdmin, setNewAdmin] = useState({
    role: 'ADMIN',
    isBlock: false,
  });
  const { role, isBlock } = newAdmin;

  const onChangeNewAdminForm = (event) =>
    setNewAdmin({ ...newAdmin, [event.target.name]: event.target.value });

  const currentAdmin =
    currentId.id !== 0
      ? admins.data.find((admin) => admin.id === currentId.id)
      : null;

  useEffect(() => {
    if (currentId.id !== 0) {
      setNewAdmin({
        role: currentAdmin.role,
        isBlock: currentAdmin.isBlock,
      });
      dispatch(showModal());
    } else {
      setNewAdmin({
        role: 'ADMIN',
        isBlock: false,
      });
    }
  }, [currentId, dispatch]);

  const closeDialog = () => {
    setNewAdmin({
      role: 'ADMIN',
      isBlock: false,
    });
    dispatch(hideModal());
    if (currentId.id !== 0) dispatch(setCurrentId(0));
  };

  const onSubmit = async (event) => {
    event.preventDefault();
    dispatch(
      updateAdmin.updateAdminRequest({
        id: currentId.id,
        role,
        isBlock,
      })
    );
    dispatch(
      showToast({
        message: 'Vui lòng chờ! Dữ liệu đang được cập nhật...',
        type: 'warning',
      })
    );
  };

  return (
    <Dialog TransitionComponent={Transition} open={modal.show} scroll="body">
      <DialogTitle>{currentId.id === 0 ? '' : 'CHỈNH SỬA'}</DialogTitle>
      <DialogContent dividers>
        <TextField
          margin="normal"
          required
          fullWidth
          select
          label="Kích hoạt"
          name="isBlock"
          onChange={onChangeNewAdminForm}
          value={isBlock}
        >
          <MenuItem value={false}>Kích hoạt</MenuItem>
          <MenuItem value={true}>Khoá</MenuItem>
        </TextField>
        <TextField
          margin="normal"
          required
          fullWidth
          select
          label="Quyền"
          name="role"
          onChange={onChangeNewAdminForm}
          value={role}
        >
          <MenuItem value={'USER'}>Khách hàng</MenuItem>
          <MenuItem value={'ADMIN'}>Quản trị viên</MenuItem>
        </TextField>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={onSubmit}>
          Ok
        </Button>
        <Button onClick={closeDialog}>Cancel</Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddModal;
