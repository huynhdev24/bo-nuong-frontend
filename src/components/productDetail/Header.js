const Header = ({ products }) => {
  return (
    <section
      className="breadcrumb-section set-bg"
      style={{
        backgroundImage: `url(img/breadcrumb.jpg)`,
      }}
    >
      <div className="container">
        <div className="row">
          <div className="col-lg-12 text-center">
            <div className="breadcrumb__text">
              <h2>Thông tin sản phẩm</h2>
              <div className="breadcrumb__option">
                <a href="/">Trang chủ </a>
                <a href="/products">Sản phẩm </a>
                <span>{products?.singleProduct?.name}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Header;
