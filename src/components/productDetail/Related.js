import React from 'react';
import { Link } from 'react-router-dom';

const Related = ({ products }) => {
  return (
    <section className="related-product">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="section-title related__product__title">
              <h2>Sản phẩm liên quan</h2>
            </div>
          </div>
        </div>
        <div className="row">
          {products?.relatedProducts?.map((product) => (
            <div className="col-lg-3 col-md-4 col-sm-6" key={product?.id}>
              <div className="product__item">
                <div
                  className="product__item__pic set-bg"
                  style={{
                    backgroundImage: `url(${product?.image})`,
                  }}
                >
                  <ul className="product__item__pic__hover">
                    <li>
                      <a href="/">
                        <i className="fa fa-shopping-cart"></i>
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="product__item__text">
                  <h6>
                    <Link to={`/products/${product?.id}`}>{product?.name}</Link>
                  </h6>
                  <h5>
                    {String(product?.price).replace(/(.)(?=(\d{3})+$)/g, '$1,')}{' '}
                    &#8363;
                  </h5>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Related;
