import { CircularProgress, Rating } from '@mui/material';
import Tooltip from '../layouts/Tooltip';
import { useContext, useState } from 'react';
import { useSelector } from 'react-redux';
import { CartContext } from '../../contexts/CartContext';
import { toast$ } from '../../redux/selectors';

const Detail = ({ products, comments }) => {
  const { addCart } = useContext(CartContext);
  const toast = useSelector(toast$);
  const [quantity, setQuantity] = useState(1);

  const onAddCart = (product) => {
    addCart(product, quantity);
    setQuantity(1);
  };

  const onClickDec = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    } else {
      setQuantity(1);
    }
  };

  const onClickInc = () => {
    setQuantity(quantity + 1);
  };

  let body;
  if (products.loading) {
    body = (
      <div
        style={{
          marginTop: '50px',
          textAlign: 'center',
        }}
      >
        <CircularProgress />
      </div>
    );
  } else {
    body = (
      <>
        <div className="col-lg-6 col-md-6">
          <div className="product__details__pic">
            <div className="product__details__pic__item">
              <img
                className="product__details__pic__item--large"
                src={products?.singleProduct?.image}
                alt=""
              />
            </div>
          </div>
        </div>
        <div className="col-lg-6 col-md-6">
          <div className="product__details__text">
            <h3>{products?.singleProduct?.name}</h3>
            <div className="product__details__rating">
              <Rating
                name="read-only"
                value={products?.singleProduct?.rating || 5}
                readOnly
              />
              <span>({comments.data.length} reviews)</span>
            </div>
            <div className="product__details__price">
              {String(products?.singleProduct?.price).replace(
                /(.)(?=(\d{3})+$)/g,
                '$1,'
              )}{' '}
              &#8363;
            </div>
            <p>{products?.singleProduct?.description}</p>
            <div className="product__details__quantity">
              <div className="quantity">
                <div className="pro-qty">
                  <span className="dec qtybtn" onClick={onClickDec}>
                    -
                  </span>
                  <input type="text" value={quantity} readOnly />
                  <span className="inc qtybtn" onClick={onClickInc}>
                    +
                  </span>
                </div>
              </div>
            </div>
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a
              onClick={() => onAddCart(products?.singleProduct)}
              className="primary-btn"
            >
              Thêm vào giỏ hàng
            </a>
            {/* <a href="/" className="heart-icon">
          <span className="icon_heart_alt"></span>
        </Link> */}
            <ul>
              <li>
                <b>Còn lại</b>{' '}
                <span>
                  {String(products?.singleProduct?.quantity).replace(
                    /(.)(?=(\d{3})+$)/g,
                    '$1,'
                  )}
                </span>
              </li>
              {products?.singleProduct?.discount > 0 && (
                <li>
                  <b>Giảm giá</b>{' '}
                  <span>
                    {String(products?.singleProduct?.discount).replace(
                      /(.)(?=(\d{3})+$)/g,
                      '$1,'
                    )}{' '}
                    &#8363;
                    <samp> Chỉ duy nhất hôm nay</samp>
                  </span>
                </li>
              )}
              <li>
                <b>Đơn vị tính</b> <span>{products?.singleProduct?.unit}</span>
              </li>
            </ul>
          </div>
        </div>
      </>
    );
  }

  return (
    <>
      <Tooltip toast={toast} />
      {body}
    </>
  );
};

export default Detail;
