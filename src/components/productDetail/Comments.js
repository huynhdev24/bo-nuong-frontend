import {
  Avatar,
  Box,
  Button,
  CircularProgress,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  TextField,
  Typography,
  Rating,
} from '@mui/material';
import moment from 'moment';
import 'moment/locale/vi';
import { Fragment, useContext, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { AuthContext } from '../../contexts/AuthContext';
import { showToast } from '../../redux/actions';
import { createComment } from '../../redux/actions/comments';
moment.locale('vi');

const Comments = ({ comments, productId }) => {
  const dispatch = useDispatch();
  const [newComment, setNewComment] = useState({
    content: '',
    rating: 5,
  });
  const { content, rating } = newComment;
  const {
    authState: { user },
  } = useContext(AuthContext);

  const onChangeNewCommentForm = (event) => {
    setNewComment({ ...newComment, [event.target.name]: event.target.value });
  };

  const onSubmit = async (event) => {
    event.preventDefault();
    dispatch(
      createComment.createCommentRequest({
        ...newComment,
        rating: parseInt(rating),
        productId,
        userId: user.id,
      })
    );
    dispatch(
      showToast({
        message: 'Vui lòng chờ! Dữ liệu đang được cập nhật...',
        type: 'warning',
      })
    );
    setNewComment({
      content: '',
      rating: 5,
    });
  };

  let commentsComponent;
  if (comments.loading) {
    commentsComponent = (
      <div
        style={{
          marginTop: '50px',
          textAlign: 'center',
        }}
      >
        <CircularProgress />
      </div>
    );
  } else {
    if (comments.data.length !== 0)
      commentsComponent = comments.data.map((item) => (
        <Fragment key={item.id}>
          <ListItem alignItems="flex-start">
            <ListItemAvatar>
              <Avatar
                alt={item.user?.fullName ?? 'img'}
                src={item.user?.avatar ?? ''}
              />
            </ListItemAvatar>
            <ListItemText
              primary={item.user?.fullName ?? 'user'}
              secondary={
                <Fragment>
                  {`${moment(item.createdAt ?? '2001-01-21').format('lll')} - `}
                  <Typography
                    sx={{ display: 'inline' }}
                    component="span"
                    variant="body2"
                    color="text.primary"
                  >
                    {item.content}
                  </Typography>
                </Fragment>
              }
            />
            <Rating name="read-only" value={item?.rating} readOnly />
          </ListItem>
          <Divider variant="inset" component="li" />
        </Fragment>
      ));
    else
      commentsComponent = (
        <div style={{ textAlign: 'center' }}>
          <Typography variant="body1" color="text.primary">
            Chưa có bình luận
          </Typography>
        </div>
      );
  }

  return (
    <>
      <Typography variant="h6" component="h6" sx={{ mt: 4, mb: 1, ml: 4 }}>
        Bình luận
      </Typography>
      {user ? (
        <Box
          component="form"
          onSubmit={onSubmit}
          sx={{ width: '98%', m: 'auto' }}
        >
          <div style={{ marginLeft: '20px', display: 'inline-flex' }}>
            <Typography component="legend" sx={{ mr: 3 }}>
              Đánh giá
            </Typography>
            <Rating
              value={rating}
              name="rating"
              onChange={onChangeNewCommentForm}
            />
          </div>
          <TextField
            margin="dense"
            multiline
            rows={4}
            required
            fullWidth
            label="Đừng ngại để lại bình luận"
            name="content"
            value={content}
            onChange={onChangeNewCommentForm}
          />
          <Button
            type="submit"
            variant="outlined"
            sx={{ mb: 2, float: 'right' }}
          >
            Gửi bình luận
          </Button>
        </Box>
      ) : (
        <Typography sx={{ mx: 'auto', textAlign: 'center', mt: 1, px: 1 }}>
          Đăng nhập tài khoản để đăng bình luận!{' '}
          <Link to="/login" className="hover-link">
            Đăng nhập ngay
          </Link>
        </Typography>
      )}
      <List sx={{ width: '100%', bgcolor: 'background.paper', mt: '100px' }}>
        {commentsComponent}
      </List>
    </>
  );
};

export default Comments;
