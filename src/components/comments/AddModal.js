/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Rating,
  Typography,
} from '@mui/material';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  hideModal,
  setCurrentId,
  showModal,
  showToast,
} from '../../redux/actions';
import { updateComment } from '../../redux/actions/comments';
import { currentId$, comments$, modal$, toast$ } from '../../redux/selectors';
import AlertMessage from '../layouts/AlertMessage';
import Transition from '../layouts/Transition';

const AddModal = () => {
  const dispatch = useDispatch();
  const [alert, setAlert] = useState(null);
  const modal = useSelector(modal$);
  const currentId = useSelector(currentId$);
  const comments = useSelector(comments$);
  const toast = useSelector(toast$);
  const [newComment, setNewComment] = useState({
    content: '',
    rating: 5,
    productId: '',
    userId: '',
  });
  const { content, rating } = newComment;
  const currentComment =
    currentId.id !== 0
      ? comments.data.find((comment) => comment.id === currentId.id)
      : null;

  useEffect(() => {
    if (currentId.id !== 0) {
      setNewComment({
        content: currentComment?.content,
        rating: currentComment?.rating,
        productId: currentComment?.product.id,
        userId: currentComment?.user.id,
      });
      dispatch(showModal());
    } else {
      setNewComment({
        content: '',
        rating: 5,
        productId: '',
        userId: '',
      });
    }
  }, [currentId, dispatch]);

  const onChangeNewCommentForm = (event) =>
    setNewComment({ ...newComment, [event.target.name]: event.target.value });

  const closeDialog = () => {
    setNewComment({
      content: '',
      rating: 5,
      productId: '',
      userId: '',
    });
    dispatch(hideModal());
    if (currentId.id !== 0) dispatch(setCurrentId(0));
  };

  const onSubmit = async (event) => {
    event.preventDefault();
    if (!content) {
      setAlert({
        type: 'warning',
        message: 'Nội dung bị bỏ trống.',
      });
      setTimeout(() => setAlert(null), 5000);
      return;
    }
    setAlert(null);
    if (currentId.id !== 0) {
      dispatch(
        updateComment.updateCommentRequest({
          id: currentId.id,
          rating: parseInt(rating),
          ...newComment,
        })
      );
      dispatch(
        showToast({
          message: 'Vui lòng chờ! Dữ liệu đang được cập nhật...',
          type: 'warning',
        })
      );
    }
  };

  return (
    <Dialog TransitionComponent={Transition} open={modal.show} scroll="body">
      <DialogTitle>CHỈNH SỬA</DialogTitle>
      <DialogContent dividers>
        {alert && <AlertMessage info={alert} />}
        {!alert && <AlertMessage info={toast} />}
        <TextField
          margin="dense"
          label="Nội dung"
          type="text"
          name="content"
          required
          fullWidth
          variant="standard"
          value={content}
          onChange={onChangeNewCommentForm}
        />
        <Typography component="legend" sx={{ mr: 3, mt: 2, color: '#65748B' }}>
          Đánh giá
        </Typography>
        <Rating
          value={rating}
          name="rating"
          onChange={onChangeNewCommentForm}
        />
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={onSubmit}>
          Ok
        </Button>
        <Button onClick={closeDialog}>Cancel</Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddModal;
