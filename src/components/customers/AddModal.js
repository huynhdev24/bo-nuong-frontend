/* eslint-disable no-useless-escape */
/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  MenuItem,
  TextField,
} from '@mui/material';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  hideModal,
  setCurrentId,
  showModal,
  showToast,
} from '../../redux/actions';
import { updateCustomer } from '../../redux/actions/customers';
import { currentId$, customers$, modal$ } from '../../redux/selectors';
import Transition from '../layouts/Transition';

const AddModal = () => {
  const dispatch = useDispatch();
  const modal = useSelector(modal$);
  const customers = useSelector(customers$);
  const currentId = useSelector(currentId$);
  const [newCustomer, setNewCustomer] = useState({
    role: 'USER',
    isBlock: false,
  });
  const { role, isBlock } = newCustomer;

  const onChangeNewCustomerForm = (event) =>
    setNewCustomer({ ...newCustomer, [event.target.name]: event.target.value });

  const currentCustomer =
    currentId.id !== 0
      ? customers.data.find((customer) => customer.id === currentId.id)
      : null;

  useEffect(() => {
    if (currentId.id !== 0) {
      setNewCustomer({
        role: currentCustomer.role,
        isBlock: currentCustomer.isBlock,
      });
      dispatch(showModal());
    } else {
      setNewCustomer({
        role: 'USER',
        isBlock: false,
      });
    }
  }, [currentId, dispatch]);

  const closeDialog = () => {
    setNewCustomer({
      role: 'USER',
      isBlock: false,
    });
    dispatch(hideModal());
    if (currentId.id !== 0) dispatch(setCurrentId(0));
  };

  const onSubmit = async (event) => {
    event.preventDefault();
    dispatch(
      updateCustomer.updateCustomerRequest({
        id: currentId.id,
        role,
        isBlock,
      })
    );
    dispatch(
      showToast({
        message: 'Vui lòng chờ! Dữ liệu đang được cập nhật...',
        type: 'warning',
      })
    );
  };

  return (
    <Dialog TransitionComponent={Transition} open={modal.show} scroll="body">
      <DialogTitle>{currentId.id === 0 ? '' : 'CHỈNH SỬA'}</DialogTitle>
      <DialogContent dividers>
        <TextField
          margin="normal"
          required
          fullWidth
          select
          label="Kích hoạt"
          name="isBlock"
          onChange={onChangeNewCustomerForm}
          value={isBlock}
        >
          <MenuItem value={false}>Kích hoạt</MenuItem>
          <MenuItem value={true}>Khoá</MenuItem>
        </TextField>
        <TextField
          margin="normal"
          required
          fullWidth
          select
          label="Quyền"
          name="role"
          onChange={onChangeNewCustomerForm}
          value={role}
        >
          <MenuItem value={'USER'}>Khách hàng</MenuItem>
          <MenuItem value={'ADMIN'}>Quản trị viên</MenuItem>
        </TextField>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={onSubmit}>
          Ok
        </Button>
        <Button onClick={closeDialog}>Cancel</Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddModal;
