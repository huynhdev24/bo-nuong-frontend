import { ThemeProvider } from '@mui/material/styles';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Layout from './components/layouts/Layout';
import AdminRoute from './components/routings/AdminRoute';
import Auth from './components/routings/Auth';
import AuthContextProvider from './contexts/AuthContext';
import CartContextProvider from './contexts/CartContext';
import { theme } from './theme';
import AdminAdmins from './views/admin/AdminAdmins';
import AdminCategories from './views/admin/AdminCategories';
import AdminComments from './views/admin/AdminComments';
import AdminCustomers from './views/admin/AdminCustomers';
import AdminOrders from './views/admin/AdminOrders';
import AdminProducts from './views/admin/AdminProducts';
import Revenue from './views/admin/Revenue';
import Cart from './views/Cart';
import Checkout from './views/Checkout';
import CheckoutForm from './views/checkout/CheckoutForm';
import PaymentFailed from './views/checkout/PaymentFailed';
import Contact from './views/Contact';
import Error from './views/Error';
import Home from './views/Home';
import OrdersHistory from './views/OrdersHistory';
import PersonalInfo from './views/PersonalInfo';
import ProductDetail from './views/ProductDetail';
import Products from './views/Products';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <AuthContextProvider>
        <CartContextProvider>
          <Router>
            <Switch>
              <Layout exact path="/" component={Home} />
              <Layout exact path="/contact" component={Contact} />
              <Layout exact path="/products" component={Products} />
              <Layout exact path="/products/:id" component={ProductDetail} />
              <Layout exact path="/cart" component={Cart} />
              <Layout exact path="/checkout" component={Checkout} />
              <Layout exact path="/orders-history" component={OrdersHistory} />
              <Layout exact path="/personal-info" component={PersonalInfo} />
              <Route exact path="/checkout/payment" component={CheckoutForm} />
              <Route exact path="/payment/failed" component={PaymentFailed} />
              <Route
                exact
                path="/payment/success"
                render={(props) => (
                  <Auth {...props} authRoute="payment-success" />
                )}
              />
              <Route
                exact
                path="/login"
                render={(props) => <Auth {...props} authRoute="login" />}
              />
              <Route
                exact
                path="/register"
                render={(props) => <Auth {...props} authRoute="register" />}
              />
              <Route
                exact
                path="/activate-account"
                render={(props) => <Auth {...props} authRoute="activate" />}
              />
              <Route
                exact
                path="/reset-password"
                render={(props) => <Auth {...props} authRoute="reset" />}
              />
              <AdminRoute
                exact
                path="/admin/products"
                component={AdminProducts}
              />
              <AdminRoute
                exact
                path="/admin/categories"
                component={AdminCategories}
              />
              <AdminRoute
                exact
                path="/admin/customers"
                component={AdminCustomers}
              />
              <AdminRoute
                exact
                path="/admin/comments"
                component={AdminComments}
              />
              <AdminRoute exact path="/admin/orders" component={AdminOrders} />
              <AdminRoute exact path="/admin/revenue" component={Revenue} />
              <AdminRoute exact path="/admin/admins" component={AdminAdmins} />
              <Route path="/:someString" component={Error} />
            </Switch>
          </Router>
        </CartContextProvider>
      </AuthContextProvider>
    </ThemeProvider>
  );
}

export default App;
