export const apiUrl =
  process.env.NODE_ENV !== 'production'
    ? 'http://localhost:8080'
    : 'http://localhost:8080';
export const LOCAL_STORAGE_TOKEN_NAME = 'token';
export const LOAD_SUCCESS = 'LOAD_SUCCESS';
export const LOCAL_STORAGE_CART_NAME = 'cart';
