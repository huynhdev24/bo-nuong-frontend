import { createContext, useEffect, useReducer } from 'react';
import { useDispatch } from 'react-redux';
import { showToast } from '../redux/actions';
import { LOAD_SUCCESS, LOCAL_STORAGE_CART_NAME } from '../constants';
import { cartReducer } from '../reducers/cartReducer';

export const CartContext = createContext();

const CartContextProvider = ({ children }) => {
  const [cartState, dispatch] = useReducer(cartReducer, {
    cartLoading: true,
    cart: null,
  });
  const dispatchRedux = useDispatch();

  const getCart = () => {
    let productsString = localStorage.getItem(LOCAL_STORAGE_CART_NAME);
    let products = [];
    if (productsString) {
      products = JSON.parse(productsString);
    }
    return products;
  };

  const loadCart = () => {
    if (localStorage[LOCAL_STORAGE_CART_NAME]) {
      const cart = getCart();
      dispatch({
        type: LOAD_SUCCESS,
        payload: { cart },
      });
    } else {
      dispatch({
        type: LOAD_SUCCESS,
        payload: { cart: null },
      });
    }
  };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => loadCart(), []);

  const addCart = (product, newQuantity) => {
    const products = getCart();
    const productExisted = products.find((item) => {
      if (item.product.id === product.id) {
        if (newQuantity) item.quantity += newQuantity;
        else item.quantity++;
        item.total =
          (item.product.price - item.product.discount) * item.quantity;
        return true;
      }
      return false;
    });
    if (!productExisted) {
      const updateQuantity = newQuantity ?? 1;
      const newProduct = {
        product,
        quantity: updateQuantity,
        total: (product.price - product.discount) * updateQuantity,
      };
      if (newQuantity) products.push(newProduct);
      else products.push(newProduct);
    }
    localStorage.setItem(LOCAL_STORAGE_CART_NAME, JSON.stringify(products));
    loadCart();
    dispatchRedux(
      showToast({
        message: 'Đã thêm vào giỏ hàng',
        type: 'success',
      })
    );
  };

  const deleteCart = () => {
    localStorage.removeItem(LOCAL_STORAGE_CART_NAME);
    dispatch({
      type: LOAD_SUCCESS,
      payload: { cart: null },
    });
  };

  const deleteCartItem = (product) => {
    const products = getCart();
    const newProduct = products.filter(
      (item) => item.product.id !== product.id
    );
    localStorage.setItem(LOCAL_STORAGE_CART_NAME, JSON.stringify(newProduct));
    loadCart();
  };

  const updateCart = (index, newQuantity) => {
    const products = getCart();
    products[index].quantity = newQuantity;
    products[index].total =
      (products[index].product.price - products[index].product.discount) *
      products[index].quantity;
    localStorage.setItem(LOCAL_STORAGE_CART_NAME, JSON.stringify(products));
    loadCart();
  };

  const cartContextData = {
    loadCart,
    addCart,
    deleteCart,
    deleteCartItem,
    updateCart,
    cartState,
  };

  return (
    <CartContext.Provider value={cartContextData}>
      {children}
    </CartContext.Provider>
  );
};

export default CartContextProvider;
