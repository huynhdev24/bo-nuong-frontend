import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import './index.css';
import App from './App';
import reducers from './redux/reducers';
import mySaga from './redux/sagas';
import reportWebVitals from './reportWebVitals';
import { HelmetProvider, Helmet } from 'react-helmet-async';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(mySaga);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <HelmetProvider>
        <Helmet>
          <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
          <script type="text/javascript" src="/js/bootstrap.min.js"></script>
          <script
            type="text/javascript"
            src="/js/jquery.nice-select.min.js"
          ></script>
          <script type="text/javascript" src="/js/jquery-ui.min.js"></script>
          <script type="text/javascript" src="/js/jquery.slicknav.js"></script>
          <script type="text/javascript" src="/js/mixitup.min.js"></script>
          <script type="text/javascript" src="/js/owl.carousel.min.js"></script>
          <script type="text/javascript" src="/js/main.js"></script>
        </Helmet>
        <App />
      </HelmetProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
