import { createActions } from 'redux-actions';

export const getType = (reduxAction) => {
  return reduxAction.type;
};

export const getAdmins = createActions({
  getAdminsRequest: (payload) => payload,
  getAdminsSuccess: (payload) => payload,
  getAdminsFailure: (error) => error,
});

export const updateAdmin = createActions({
  updateAdminRequest: (payload) => payload,
  updateAdminSuccess: (payload) => payload,
  updateAdminFailure: (error) => error,
});
