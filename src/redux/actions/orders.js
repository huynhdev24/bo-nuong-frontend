import { createActions } from 'redux-actions';

export const getType = (reduxAction) => {
  return reduxAction.type;
};

export const getOrders = createActions({
  getOrdersRequest: (payload) => payload,
  getOrdersSuccess: (payload) => payload,
  getOrdersFailure: (error) => error,
});

export const createOrder = createActions({
  createOrderRequest: (payload) => payload,
  createOrderSuccess: (payload) => payload,
  createOrderFailure: (error) => error,
});

export const updateOrder = createActions({
  updateOrderRequest: (payload) => payload,
  updateOrderSuccess: (payload) => payload,
  updateOrderFailure: (error) => error,
});

export const getUserOrders = createActions({
  getUserOrdersRequest: (payload) => payload,
  getUserOrdersSuccess: (payload) => payload,
  getUserOrdersFailure: (error) => error,
});
