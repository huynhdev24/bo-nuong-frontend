import { createActions } from 'redux-actions';

export const getType = (reduxAction) => {
  return reduxAction.type;
};

export const getCustomers = createActions({
  getCustomersRequest: (payload) => payload,
  getCustomersSuccess: (payload) => payload,
  getCustomersFailure: (error) => error,
});

// export const createCustomer = createActions({
//   createCustomerRequest: (payload) => payload,
//   createCustomerSuccess: (payload) => payload,
//   createCustomerFailure: (error) => error,
// });

export const updateCustomer = createActions({
  updateCustomerRequest: (payload) => payload,
  updateCustomerSuccess: (payload) => payload,
  updateCustomerFailure: (error) => error,
});

// export const deleteCustomer = createActions({
//   deleteCustomerRequest: (payload) => payload,
//   deleteCustomerSuccess: (payload) => payload,
//   deleteCustomerFailure: (error) => error,
// });

export const updatePersonal = createActions({
  updatePersonalRequest: (payload) => payload,
  updatePersonalSuccess: (payload) => payload,
  updatePersonalFailure: (error) => error,
});
