import { createActions } from 'redux-actions';

export const getType = (reduxAction) => {
  return reduxAction.type;
};

export const getCategories = createActions({
  getCategoriesRequest: (payload) => payload,
  getCategoriesSuccess: (payload) => payload,
  getCategoriesFailure: (error) => error,
});

export const createCategory = createActions({
  createCategoryRequest: (payload) => payload,
  createCategorySuccess: (payload) => payload,
  createCategoryFailure: (error) => error,
});

export const updateCategory = createActions({
  updateCategoryRequest: (payload) => payload,
  updateCategorySuccess: (payload) => payload,
  updateCategoryFailure: (error) => error,
});

export const deleteCategory = createActions({
  deleteCategoryRequest: (payload) => payload,
  deleteCategorySuccess: (payload) => payload,
  deleteCategoryFailure: (error) => error,
});
