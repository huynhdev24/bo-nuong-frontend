import { createActions } from 'redux-actions';

export const getType = (reduxAction) => {
  return reduxAction.type;
};

export const getProducts = createActions({
  getProductsRequest: (payload) => payload,
  getProductsSuccess: (payload) => payload,
  getProductsFailure: (error) => error,
});

export const getLatestProducts = createActions({
  getLatestProductsRequest: (payload) => payload,
  getLatestProductsSuccess: (payload) => payload,
  getLatestProductsFailure: (error) => error,
});

export const getTopRatingProducts = createActions({
  getTopRatingProductsRequest: (payload) => payload,
  getTopRatingProductsSuccess: (payload) => payload,
  getTopRatingProductsFailure: (error) => error,
});

export const getRelatedProducts = createActions({
  getRelatedProductsRequest: (payload) => payload,
  getRelatedProductsSuccess: (payload) => payload,
  getRelatedProductsFailure: (error) => error,
});

export const getDiscountProducts = createActions({
  getDiscountProductsRequest: (payload) => payload,
  getDiscountProductsSuccess: (payload) => payload,
  getDiscountProductsFailure: (error) => error,
});

export const getProductDetail = createActions({
  getProductDetailRequest: (payload) => payload,
  getProductDetailSuccess: (payload) => payload,
  getProductDetailFailure: (error) => error,
});

export const createProduct = createActions({
  createProductRequest: (payload) => payload,
  createProductSuccess: (payload) => payload,
  createProductFailure: (error) => error,
});

export const updateProduct = createActions({
  updateProductRequest: (payload) => payload,
  updateProductSuccess: (payload) => payload,
  updateProductFailure: (error) => error,
});

export const deleteProduct = createActions({
  deleteProductRequest: (payload) => payload,
  deleteProductSuccess: (payload) => payload,
  deleteProductFailure: (error) => error,
});
