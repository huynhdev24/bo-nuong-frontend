import { call, put } from 'redux-saga/effects';
import * as api from '../../api';
import { hideModal, setCurrentId, showToast } from '../actions';
import { getAdmins, updateAdmin } from '../actions/admins';

export function* getAdminsSaga(action) {
  try {
    const response = yield call(api.getUsers, action.payload);
    yield put(getAdmins.getAdminsSuccess(response.data.users));
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(getAdmins.getAdminsFailure(error.response.data));
  }
}

export function* updateAdminSaga(action) {
  try {
    const response = yield call(api.updateUser, action.payload);
    yield put(updateAdmin.updateAdminSuccess(response.data.user));
    yield put(hideModal());
    yield put(setCurrentId(0));
    yield put(
      showToast({
        message: response.data.message ? response.data.message : 'Lỗi máy chủ',
        type: response.data.success ? 'success' : 'error',
      })
    );
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(updateAdmin.updateAdminFailure(error.response.data));
  }
}
