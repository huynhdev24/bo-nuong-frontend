import { takeLatest } from 'redux-saga/effects';
import {
  createCategory,
  deleteCategory,
  getCategories,
  updateCategory,
} from '../actions/categories';
import {
  createCategorySaga,
  deleteCategorySaga,
  getCategoriesSaga,
  updateCategorySaga,
} from './categories';

import {
  getProducts,
  getLatestProducts,
  getTopRatingProducts,
  getRelatedProducts,
  createProduct,
  deleteProduct,
  getProductDetail,
  updateProduct,
  getDiscountProducts,
} from '../actions/products';
import {
  getProductsSaga,
  getLatestProductsSaga,
  getTopRatingProductsSaga,
  getRelatedProductsSaga,
  createProductSaga,
  deleteProductSaga,
  getProductDetailSaga,
  updateProductSaga,
  getDiscountProductsSaga,
} from './products';

import {
  //createCustomer,
  //deleteCustomer,
  getCustomers,
  updateCustomer,
  updatePersonal,
} from '../actions/customers';
import {
  //createCustomerSaga,
  //deleteCustomerSaga,
  getCustomersSaga,
  updateCustomerSaga,
  updatePersonalSaga,
} from './customers';

import {
  getProductComments,
  createComment,
  getAllComments,
  updateComment,
  deleteComment,
} from '../actions/comments';
import {
  getProductCommentsSaga,
  createCommentSaga,
  getAllCommentsSaga,
  updateCommentSaga,
  deleteCommentSaga,
} from './comments';

import {
  createOrder,
  getOrders,
  updateOrder,
  getUserOrders,
} from '../actions/orders';
import {
  createOrderSaga,
  getOrdersSaga,
  updateOrderSaga,
  getUserOrdersSaga,
} from './orders';

import { getAdmins, updateAdmin } from '../actions/admins';
import { getAdminsSaga, updateAdminSaga } from './admins';

function* mySaga() {
  yield takeLatest(getCategories.getCategoriesRequest, getCategoriesSaga);
  yield takeLatest(createCategory.createCategoryRequest, createCategorySaga);
  yield takeLatest(updateCategory.updateCategoryRequest, updateCategorySaga);
  yield takeLatest(deleteCategory.deleteCategoryRequest, deleteCategorySaga);

  yield takeLatest(getProducts.getProductsRequest, getProductsSaga);
  yield takeLatest(
    getLatestProducts.getLatestProductsRequest,
    getLatestProductsSaga
  );
  yield takeLatest(
    getTopRatingProducts.getTopRatingProductsRequest,
    getTopRatingProductsSaga
  );
  yield takeLatest(
    getDiscountProducts.getDiscountProductsRequest,
    getDiscountProductsSaga
  );
  yield takeLatest(
    getRelatedProducts.getRelatedProductsRequest,
    getRelatedProductsSaga
  );
  yield takeLatest(
    getProductDetail.getProductDetailRequest,
    getProductDetailSaga
  );
  yield takeLatest(createProduct.createProductRequest, createProductSaga);
  yield takeLatest(updateProduct.updateProductRequest, updateProductSaga);
  yield takeLatest(deleteProduct.deleteProductRequest, deleteProductSaga);

  yield takeLatest(getCustomers.getCustomersRequest, getCustomersSaga);
  //yield takeLatest(createCustomer.createCustomerRequest, createCustomerSaga);
  yield takeLatest(updateCustomer.updateCustomerRequest, updateCustomerSaga);
  //yield takeLatest(deleteCustomer.deleteCustomerRequest, deleteCustomerSaga);
  yield takeLatest(updatePersonal.updatePersonalRequest, updatePersonalSaga);

  yield takeLatest(
    getProductComments.getProductCommentsRequest,
    getProductCommentsSaga
  );
  yield takeLatest(createComment.createCommentRequest, createCommentSaga);
  yield takeLatest(getAllComments.getAllCommentsRequest, getAllCommentsSaga);
  yield takeLatest(updateComment.updateCommentRequest, updateCommentSaga);
  yield takeLatest(deleteComment.deleteCommentRequest, deleteCommentSaga);

  yield takeLatest(createOrder.createOrderRequest, createOrderSaga);
  yield takeLatest(getOrders.getOrdersRequest, getOrdersSaga);
  yield takeLatest(updateOrder.updateOrderRequest, updateOrderSaga);
  yield takeLatest(getUserOrders.getUserOrdersRequest, getUserOrdersSaga);

  yield takeLatest(getAdmins.getAdminsRequest, getAdminsSaga);
  yield takeLatest(updateAdmin.updateAdminRequest, updateAdminSaga);
}

export default mySaga;
