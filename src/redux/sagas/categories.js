import { call, put } from 'redux-saga/effects';
import * as api from '../../api';
import { hideModal, setCurrentId, showToast } from '../actions';
import {
  createCategory,
  deleteCategory,
  getCategories,
  updateCategory,
} from '../actions/categories';

export function* getCategoriesSaga(action) {
  try {
    const response = yield call(api.getCategories, action.payload);
    yield put(getCategories.getCategoriesSuccess(response.data.categories));
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(getCategories.getCategoriesFailure(error.response.data));
  }
}

export function* createCategorySaga(action) {
  try {
    const response = yield call(api.createCategory, action.payload);
    yield put(createCategory.createCategorySuccess(response.data.category));
    yield put(hideModal());
    yield put(setCurrentId(0));
    yield put(
      showToast({
        message: response.data.message ? response.data.message : 'Lỗi máy chủ',
        type: response.data.success ? 'success' : 'error',
      })
    );
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(createCategory.createCategoryFailure(error.response.data));
  }
}

export function* updateCategorySaga(action) {
  try {
    const response = yield call(api.updateCategory, action.payload);
    yield put(updateCategory.updateCategorySuccess(response.data.category));
    yield put(hideModal());
    yield put(setCurrentId(0));
    yield put(
      showToast({
        message: response.data.message ? response.data.message : 'Lỗi máy chủ',
        type: response.data.success ? 'success' : 'error',
      })
    );
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(updateCategory.updateCategoryFailure(error.response.data));
  }
}

export function* deleteCategorySaga(action) {
  try {
    const response = yield call(api.deleteCategory, action.payload);
    if (response.data.success) {
      yield put(deleteCategory.deleteCategorySuccess(response.data.category));
    }
    yield put(hideModal());
    yield put(setCurrentId(0));
    yield put(
      showToast({
        message: response.data.message ? response.data.message : 'Lỗi máy chủ',
        type: response.data.success ? 'success' : 'error',
      })
    );
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(deleteCategory.deleteCategoryFailure(error.response.data));
  }
}
