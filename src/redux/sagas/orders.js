import { call, put } from 'redux-saga/effects';
import * as api from '../../api';
import { showToast, hideModal, setCurrentId } from '../actions';
import {
  createOrder,
  getOrders,
  updateOrder,
  getUserOrders,
} from '../actions/orders';

export function* createOrderSaga(action) {
  try {
    const response = yield call(api.createOrder, action.payload);
    yield put(createOrder.createOrderSuccess(response.data.order));
    yield put(
      showToast({
        message: response.data.message ? response.data.message : 'Lỗi máy chủ',
        type: response.data.success ? 'success' : 'error',
      })
    );
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(createOrder.createOrderFailure(error.response.data));
  }
}

export function* getOrdersSaga(action) {
  try {
    const response = yield call(api.getOrders, action.payload);
    yield put(getOrders.getOrdersSuccess(response.data.orders));
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(getOrders.getOrdersFailure(error.response.data));
  }
}

export function* updateOrderSaga(action) {
  try {
    const response = yield call(api.updateOrder, action.payload);
    yield put(updateOrder.updateOrderSuccess(response.data.order));
    yield put(hideModal());
    yield put(setCurrentId(0));
    yield put(
      showToast({
        message: response.data.message ? response.data.message : 'Lỗi máy chủ',
        type: response.data.success ? 'success' : 'error',
      })
    );
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(updateOrder.updateOrderFailure(error.response.data));
  }
}

export function* getUserOrdersSaga(action) {
  try {
    const response = yield call(api.getUserOrders, action.payload);
    yield put(getUserOrders.getUserOrdersSuccess(response.data.orders));
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(getUserOrders.getUserOrdersFailure(error.response.data));
  }
}
