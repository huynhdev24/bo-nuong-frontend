import { call, put } from 'redux-saga/effects';
import * as api from '../../api';
import { showToast, hideModal, setCurrentId } from '../actions';
import {
  createComment,
  getProductComments,
  getAllComments,
  updateComment,
  deleteComment,
} from '../actions/comments';

export function* getProductCommentsSaga(action) {
  try {
    const response = yield call(api.getProductComments, action.payload);
    yield put(
      getProductComments.getProductCommentsSuccess(response.data.comments)
    );
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(
      getProductComments.getProductCommentsFailure(error.response.data)
    );
  }
}

export function* createCommentSaga(action) {
  try {
    const response = yield call(api.createComment, action.payload);
    yield put(createComment.createCommentSuccess(response.data.comment));
    yield put(
      showToast({
        message: response.data.message ? response.data.message : 'Lỗi máy chủ',
        type: response.data.success ? 'success' : 'error',
      })
    );
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(createComment.createCommentFailure(error.response.data));
  }
}

export function* getAllCommentsSaga(action) {
  try {
    const response = yield call(api.getAllComments, action.payload);
    yield put(getAllComments.getAllCommentsSuccess(response.data.comments));
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(getAllComments.getAllCommentsFailure(error.response.data));
  }
}

export function* updateCommentSaga(action) {
  try {
    const response = yield call(api.updateComment, action.payload);
    yield put(updateComment.updateCommentSuccess(response.data.comment));
    yield put(hideModal());
    yield put(setCurrentId(0));
    yield put(
      showToast({
        message: response.data.message ? response.data.message : 'Lỗi máy chủ',
        type: response.data.success ? 'success' : 'error',
      })
    );
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(updateComment.updateCommentFailure(error.response.data));
  }
}

export function* deleteCommentSaga(action) {
  try {
    const response = yield call(api.deleteComment, action.payload);
    yield put(deleteComment.deleteCommentSuccess(response.data.comment));
    yield put(hideModal());
    yield put(setCurrentId(0));
    yield put(
      showToast({
        message: response.data.message ? response.data.message : 'Lỗi máy chủ',
        type: response.data.success ? 'success' : 'error',
      })
    );
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(deleteComment.deleteCommentFailure(error.response.data));
  }
}
