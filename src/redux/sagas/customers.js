import { call, put } from 'redux-saga/effects';
import * as api from '../../api';
import { hideModal, setCurrentId, showToast } from '../actions';
import {
  //createCustomer,
  //deleteCustomer,
  getCustomers,
  updateCustomer,
} from '../actions/customers';

export function* getCustomersSaga(action) {
  try {
    const response = yield call(api.getUsers, action.payload);
    yield put(getCustomers.getCustomersSuccess(response.data.users));
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(getCustomers.getCustomersFailure(error.response.data));
  }
}

// export function* createCustomerSaga(action) {
//   try {
//     const response = yield call(api.createUser, action.payload);
//     yield put(createCustomer.createCustomerSuccess(response.data.user));
//     yield put(hideModal());
//     yield put(setCurrentId(0));
//     yield put(
//       showToast({
//         message: response.data.message ? response.data.message : 'Lỗi máy chủ',
//         type: response.data.success ? 'success' : 'error',
//       })
//     );
//   } catch (error) {
//     console.log(error);
//     yield put(
//       showToast({
//         message: error.response.data.message
//           ? error.response.data.message
//           : 'Lỗi máy chủ',
//         type: error.response.data.success ? 'error' : 'error',
//       })
//     );
//     yield put(createCustomer.createCustomerFailure(error.response.data));
//   }
// }

export function* updateCustomerSaga(action) {
  try {
    const response = yield call(api.updateUser, action.payload);
    yield put(updateCustomer.updateCustomerSuccess(response.data.user));
    yield put(hideModal());
    yield put(setCurrentId(0));
    yield put(
      showToast({
        message: response.data.message ? response.data.message : 'Lỗi máy chủ',
        type: response.data.success ? 'success' : 'error',
      })
    );
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
    yield put(updateCustomer.updateCustomerFailure(error.response.data));
  }
}

// export function* deleteCustomerSaga(action) {
//   try {
//     const response = yield call(api.deleteUser, action.payload);
//     yield put(deleteCustomer.deleteCustomerSuccess(response.data.user));
//     yield put(hideModal());
//     yield put(setCurrentId(0));
//     yield put(
//       showToast({
//         message: response.data.message ? response.data.message : 'Lỗi máy chủ',
//         type: response.data.success ? 'success' : 'error',
//       })
//     );
//   } catch (error) {
//     console.log(error);
//     yield put(
//       showToast({
//         message: error.response.data.message
//           ? error.response.data.message
//           : 'Lỗi máy chủ',
//         type: error.response.data.success ? 'error' : 'error',
//       })
//     );
//     yield put(deleteCustomer.deleteCustomerFailure(error.response.data));
//   }
// }

export function* updatePersonalSaga(action) {
  try {
    const response = yield call(api.updatePersonal, action.payload);
    yield put(
      showToast({
        message: response.data.message ? response.data.message : 'Lỗi máy chủ',
        type: response.data.success ? 'success' : 'error',
      })
    );
  } catch (error) {
    console.log(error);
    yield put(
      showToast({
        message: error.response.data.message
          ? error.response.data.message
          : 'Lỗi máy chủ',
        type: error.response.data.success ? 'error' : 'error',
      })
    );
  }
}
