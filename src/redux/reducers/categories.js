import {
  createCategory,
  deleteCategory,
  getCategories,
  getType,
  updateCategory,
} from '../actions/categories';
import { INIT_STATE } from './state';

export default function categoriesReducers(
  state = INIT_STATE.categories,
  action
) {
  const { type, payload } = action;

  switch (type) {
    case getType(getCategories.getCategoriesRequest()):
      return {
        ...state,
        loading: true,
      };

    case getType(getCategories.getCategoriesSuccess()):
      return {
        ...state,
        loading: false,
        data: payload,
      };

    case getType(getCategories.getCategoriesFailure()):
      return {
        ...state,
        loading: false,
      };

    case getType(createCategory.createCategorySuccess()):
      return {
        ...state,
        loading: false,
        data: [...state.data, payload],
      };

    case getType(createCategory.createCategoryFailure()):
      return {
        ...state,
        loading: false,
      };

    case getType(updateCategory.updateCategorySuccess()):
      return {
        ...state,
        loading: false,
        data: state.data.map((category) =>
          category.id === payload.id ? payload : category
        ),
      };

    case getType(updateCategory.updateCategoryFailure()):
      return {
        ...state,
        loading: false,
      };

    case getType(deleteCategory.deleteCategorySuccess()):
      return {
        ...state,
        loading: false,
        data: state.data.filter((category) => category.id !== payload.id),
      };

    case getType(deleteCategory.deleteCategoryFailure()):
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
