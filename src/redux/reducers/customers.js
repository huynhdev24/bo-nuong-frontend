import {
  //createCustomer,
  //deleteCustomer,
  getType,
  getCustomers,
  updateCustomer,
} from '../actions/customers';
import { INIT_STATE } from './state';

export default function customersReducers(
  state = INIT_STATE.customers,
  action
) {
  const { type, payload } = action;

  switch (type) {
    case getType(getCustomers.getCustomersRequest()):
      return {
        ...state,
        loading: true,
      };

    case getType(getCustomers.getCustomersSuccess()):
      return {
        ...state,
        loading: false,
        data: payload,
      };

    case getType(getCustomers.getCustomersFailure()):
      return {
        ...state,
        loading: false,
      };

    // case getType(createCustomer.createCustomerSuccess()):
    //   return {
    //     ...state,
    //     loading: false,
    //     data: [...state.data, payload],
    //   };

    // case getType(createCustomer.createCustomerFailure()):
    //   return {
    //     ...state,
    //     loading: false,
    //   };

    case getType(updateCustomer.updateCustomerSuccess()):
      return {
        ...state,
        loading: false,
        data: state.data
          .filter((customer) =>
            customer.id === payload.id && customer.role !== payload.role
              ? false
              : true
          )
          .map((customer) =>
            customer.id === payload.id && customer.role === payload.role
              ? payload
              : customer
          ),
      };

    case getType(updateCustomer.updateCustomerFailure()):
      return {
        ...state,
        loading: false,
      };

    // case getType(deleteCustomer.deleteCustomerSuccess()):
    //   return {
    //     ...state,
    //     loading: false,
    //     data: state.data.filter((post) => post.id !== payload.id),
    //   };

    // case getType(deleteCustomer.deleteCustomerFailure()):
    //   return {
    //     ...state,
    //     loading: false,
    //   };

    default:
      return state;
  }
}
