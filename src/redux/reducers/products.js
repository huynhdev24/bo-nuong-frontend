import {
  createProduct,
  deleteProduct,
  getProducts,
  getLatestProducts,
  getProductDetail,
  getTopRatingProducts,
  getRelatedProducts,
  getType,
  updateProduct,
  getDiscountProducts,
} from '../actions/products';
import { INIT_STATE } from './state';

export default function productsReducers(state = INIT_STATE.products, action) {
  const { type, payload } = action;

  switch (type) {
    case getType(getProducts.getProductsRequest()):
      return {
        ...state,
        loading: true,
      };

    case getType(getProducts.getProductsSuccess()):
      return {
        ...state,
        loading: false,
        data: payload,
      };

    case getType(getProducts.getProductsFailure()):
      return {
        ...state,
        loading: false,
      };

    case getType(getLatestProducts.getLatestProductsSuccess()):
      return {
        ...state,
        loading: false,
        latestProducts: payload,
      };

    case getType(getTopRatingProducts.getTopRatingProductsSuccess()):
      return {
        ...state,
        loading: false,
        topRatingProducts: payload,
      };

    case getType(getRelatedProducts.getRelatedProductsSuccess()):
      return {
        ...state,
        loading: false,
        relatedProducts: payload,
      };

    case getType(getDiscountProducts.getDiscountProductsSuccess()):
      return {
        ...state,
        loading: false,
        discountProducts: payload,
      };

    case getType(getProductDetail.getProductDetailSuccess()):
      return {
        ...state,
        loading: false,
        singleProduct: payload,
      };

    case getType(createProduct.createProductSuccess()):
      return {
        ...state,
        loading: false,
        data: [...state.data, payload],
      };

    case getType(updateProduct.updateProductSuccess()):
      return {
        ...state,
        loading: false,
        data: state.data.map((product) =>
          product.id === payload.id ? payload : product
        ),
      };

    case getType(deleteProduct.deleteProductSuccess()):
      return {
        ...state,
        loading: false,
        data: state.data.filter((product) => product.id !== payload.id),
      };

    default:
      return state;
  }
}
