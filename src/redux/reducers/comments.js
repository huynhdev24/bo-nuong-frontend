import {
  createComment,
  getProductComments,
  getAllComments,
  updateComment,
  deleteComment,
  getType,
} from '../actions/comments';
import { INIT_STATE } from './state';

export default function commentsReducers(state = INIT_STATE.comments, action) {
  const { type, payload } = action;

  switch (type) {
    case getType(getProductComments.getProductCommentsRequest()):
      return {
        ...state,
        loading: true,
      };

    case getType(getProductComments.getProductCommentsSuccess()):
      return {
        ...state,
        loading: false,
        data: payload,
      };

    case getType(getProductComments.getProductCommentsFailure()):
      return {
        ...state,
        loading: false,
      };

    case getType(createComment.createCommentSuccess()):
      return {
        ...state,
        loading: false,
        data: [payload, ...state.data],
      };

    case getType(createComment.createCommentFailure()):
      return {
        ...state,
        loading: false,
      };

    case getType(getAllComments.getAllCommentsRequest()):
      return {
        ...state,
        loading: true,
      };

    case getType(getAllComments.getAllCommentsSuccess()):
      return {
        ...state,
        loading: false,
        data: payload,
      };

    case getType(getAllComments.getAllCommentsFailure()):
      return {
        ...state,
        loading: false,
      };

    case getType(updateComment.updateCommentSuccess()):
      return {
        ...state,
        loading: false,
        data: state.data.map((comment) =>
          comment.id === payload.id ? payload : comment
        ),
      };

    case getType(updateComment.updateCommentFailure()):
      return {
        ...state,
        loading: false,
      };

    case getType(deleteComment.deleteCommentSuccess()):
      return {
        ...state,
        loading: false,
        data: state.data.filter((comment) => comment.id !== payload.id),
      };

    case getType(deleteComment.deleteCommentFailure()):
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
