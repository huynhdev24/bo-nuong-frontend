import {
  createOrder,
  getOrders,
  updateOrder,
  getType,
  getUserOrders,
} from '../actions/orders';
import { INIT_STATE } from './state';

export default function ordersReducers(state = INIT_STATE.orders, action) {
  const { type, payload } = action;

  switch (type) {
    case getType(createOrder.createOrderSuccess()):
      return {
        ...state,
        loading: false,
        data: [payload, ...state.data],
      };

    case getType(createOrder.createOrderFailure()):
      return {
        ...state,
        loading: false,
      };

    case getType(getOrders.getOrdersRequest()):
      return {
        ...state,
        loading: true,
      };

    case getType(getOrders.getOrdersSuccess()):
      return {
        ...state,
        loading: false,
        data: payload,
      };

    case getType(getOrders.getOrdersFailure()):
      return {
        ...state,
        loading: false,
      };

    case getType(updateOrder.updateOrderSuccess()):
      return {
        ...state,
        loading: false,
        data: state.data.map((order) =>
          order.id === payload.id ? payload : order
        ),
      };

    case getType(updateOrder.updateOrderFailure()):
      return {
        ...state,
        loading: false,
      };

    case getType(getUserOrders.getUserOrdersSuccess()):
      return {
        ...state,
        loading: false,
        userOrders: payload,
      };

    case getType(getUserOrders.getUserOrdersFailure()):
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
