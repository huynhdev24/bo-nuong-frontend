import { combineReducers } from 'redux';
import admins from './admins';
import categories from './categories';
import comments from './comments';
import currentId from './constants/currentId';
import modal from './constants/modal';
import toast from './constants/toast';
import customers from './customers';
import orders from './orders';
import products from './products';

export default combineReducers({
  categories,
  products,
  modal,
  toast,
  currentId,
  customers,
  comments,
  orders,
  admins,
});
