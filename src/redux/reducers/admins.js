import { getType, getAdmins, updateAdmin } from '../actions/admins';
import { INIT_STATE } from './state';

export default function adminsReducers(state = INIT_STATE.admins, action) {
  const { type, payload } = action;

  switch (type) {
    case getType(getAdmins.getAdminsRequest()):
      return {
        ...state,
        loading: true,
      };

    case getType(getAdmins.getAdminsSuccess()):
      return {
        ...state,
        loading: false,
        data: payload,
      };

    case getType(getAdmins.getAdminsFailure()):
      return {
        ...state,
        loading: false,
      };

    case getType(updateAdmin.updateAdminSuccess()):
      return {
        ...state,
        loading: false,
        data: state.data
          .filter((admin) =>
            admin.id === payload.id && admin.role !== payload.role
              ? false
              : true
          )
          .map((admin) =>
            admin.id === payload.id && admin.role === payload.role
              ? payload
              : admin
          ),
      };

    case getType(updateAdmin.updateAdminFailure()):
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
