export const INIT_STATE = {
  modal: {
    show: false,
  },
  toast: {
    show: false,
    message: '',
    type: 'success',
  },
  currentId: {
    id: 0,
  },
  categories: {
    loading: false,
    data: [],
  },
  products: {
    loading: false,
    data: [],
    singleProduct: null,
    latestProducts: [],
    topRatingProducts: [],
    relatedProducts: [],
    discountProducts: [],
  },
  customers: {
    loading: false,
    data: [],
  },
  comments: {
    loading: false,
    data: [],
  },
  orders: {
    loading: false,
    data: [],
    userOrders: [],
  },
  admins: {
    loading: false,
    data: [],
  },
};
